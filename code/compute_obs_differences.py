import argparse
import matplotlib.pyplot as plt
from collections import OrderedDict
from numpy import arange, linspace
from dolfin import *
from utils import FileReader, FileWriter, print0
from scipy.interpolate import interp1d

# Read the command line arguments
example = "Example arguments: --observations1=results/model --observations2=results/obs_assimilated --output=results/error --mesh_level=3"
parser = argparse.ArgumentParser(description=example)
parser.add_argument('--observations1', required=True, help='observations1 base file')
parser.add_argument('--observations2', required=True, help='observations2 base file')
parser.add_argument('--output', required=True, help='output base filename')
parser.add_argument('--mesh_level', required=True, help='mesh level')
args = parser.parse_args()

# Read the mesh
mesh = Mesh()
mesh_file = "assimilation_data/meshes_wide/model.hdf5"
f =  HDF5File(mpi_comm_world(), mesh_file, "r")
f.read(mesh, "Meshes/Mesh{}".format(args.mesh_level), False)

# Read observations
fobs1 = FileReader(args.observations1)
fobs2 = FileReader(args.observations2)

V = VectorFunctionSpace(mesh, "CG", 1)
obs1 = OrderedDict()
obs2 = OrderedDict()
obs_dt = 0.0370
T = 17 * obs_dt + DOLFIN_EPS
obs_t_range = arange(0, T, obs_dt)

obs1 = Function(V)
obs2 = Function(V)
error = Function(V, name="error")

ferror = FileWriter(args.output)

for t, next_t in zip(obs_t_range[:-1], obs_t_range[1:]):
    print0("Read observations for time range %f - %f." % (t, next_t))
    fobs1.read(obs1, "obs/{}".format(t))
    fobs2.read(obs2, "obs/{}".format(t))

    error.vector()[:] = obs1.vector() - obs2.vector()
    ferror.write(error, "error", t)
