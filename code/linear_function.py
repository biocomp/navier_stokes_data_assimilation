from numpy import zeros, arange, where


# sum([dolfin.Function, dolfin.Function]) results in an UFLException, hence we
# hack this for now

def sum(l):
    s = l[0]
    for c in l[1:]:
        s += c
    return s


class LinearFunction(object):

    def __init__(self, xx, yy):
        """ A class for storing piecwise linear functions
          Parameters:
          xx: An array of x-values
          yy: An array of y-values associated with the x-values
        """

        # Sanity checks
        assert len(xx) == len(yy)
        for i in range(len(xx)-1):
            assert xx[i] < xx[i+1]

        self.xx = xx
        self.yy = yy

    def update(self, x, y):
        """ Set the x value to y """
        idx = where(self.xx==x)[0][0]
        self.yy[idx] = y

    def __x_index(self, x):
        """ Returns the first index i where self.xx[i] <= x <= self.xx[i+1] """

        # Sanity check
        assert self.xx[0] <= x <= self.xx[-1]

        for i in range(len(self.xx)-1):
            if self.xx[i] <= x <= self.xx[i+1]:
                return i


    def _eval_weights(self, x):
        """ Return the weights for evaluating the function at x """

        idx = self.__x_index(x)

        weights = zeros(len(self.xx))
        weights[idx] = (self.xx[idx+1] - x) / (self.xx[idx+1] - self.xx[idx])
        weights[idx+1] = 1. - weights[idx]

        return weights


    def __call__(self, x):
        """ Evaluate the function at x """

        weights = self._eval_weights(x)
        return sum([float(w)*v for w, v in zip(weights, self.yy) if w != 0])

    def _integrate_weights(self, a, b):
        """ Return the weights for integrating the function from a to b """

        # Sanity check
        assert a <= b

        a_idx = self.__x_index(a)
        b_idx = self.__x_index(b)

        if a_idx == b_idx:
            # a and b are in the same xx interval

            dx = b - a
            weights = 0.5 * self._eval_weights(a) * dx
            weights += 0.5 * self._eval_weights(b) * dx
            return weights

        else:
            # a and b are in different xx intervals

            _b = self.xx[a_idx+1]
            dx = _b - a
            weights = 0.5 * self._eval_weights(a) * dx
            weights += 0.5 * self._eval_weights(_b) * dx

            _a = self.xx[b_idx]
            dx = b - _a
            weights += 0.5 * self._eval_weights(_a) * dx
            weights += 0.5 * self._eval_weights(b) * dx

        # Intermediate time levels
        for idx in arange(a_idx+1, b_idx):
            _a = self.xx[idx]
            _b = self.xx[idx+1]
            dx = _b - _a
            weights += 0.5 * self._eval_weights(_a) * dx
            weights += 0.5 * self._eval_weights(_b) * dx

        return weights

    def integrate(self, a, b):
        """  Integrate the function from a to b """

        weights = self._integrate_weights(a, b)
        return sum([float(w)*v for w, v in zip(weights, self.yy) if w != 0])



if __name__ == "__main__":
    import numpy

    xx = [0, 1, 2, 2.5]
    yy = [2, 0, 1, 1]

    fun = LinearFunction(xx, yy)

    # Test evaluation
    assert fun(0.0) == 2.0
    assert fun(1.0) == 0.0
    assert fun(2.0) == 1.0
    assert fun(0.5) == 1.0
    assert fun(0.75) == 0.5

    # Test integration
    assert fun.integrate(0, 1) == 1.
    assert fun.integrate(0.5, 0.5) == 0.
    assert fun.integrate(0.25, 0.75) == 0.5

    assert fun.integrate(0, 2) == 1.5
    assert fun.integrate(0, 1.5) == 1.125
    assert fun.integrate(0.5, 1.5) == 0.375

    assert fun.integrate(0, 2.5) == 2.
    assert fun.integrate(0.5, 2.5) == 1.25
