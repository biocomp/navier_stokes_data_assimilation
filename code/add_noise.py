import subprocess
import argparse
from collections import namedtuple
from dolfin import Function
from problem import *
from utils import FileReader, FileWriter

############### Read the command line arguments #################
parser = argparse.ArgumentParser()
parser.add_argument('--problem', required=True, choices=["Bifurcation2D", "Aneurysm2D", "Dog3D"], help='name of the probem')
parser.add_argument('--infile', required=True, help='path to observation file')
parser.add_argument('--outfile', required=True, help='path to noisy observation file')
parser.add_argument('--intensity', required=True, type = float,
        help='standard deviation for additive Gaussian noise')
parser.add_argument('--seed', required=False, type = int, default = 0,
        help='seed for numpy RNG')

args = parser.parse_args()

# Initializee the RNG
from numpy import random
random.seed(args.seed)

fr_obs = FileReader(args.infile)
fw_obs_noisy = FileWriter(args.outfile)

sys.argv = ["", "--output_dir", "_", "--problem", args.problem]
problem = create_problem_from_commandline("generate")

# Note: Storing the random noise in a dolfin vector before adding it
# appears to the fastest and most reliable way of adding the noise.
# adding it directly to the vector of u produces unexpected results.
u = Function(problem.observation_space)
v = Function(problem.observation_space)
y = v.vector()
x = u.vector()

# We want to compute the signal-to-noise ratio

P_s = []
P_n = []
for t in problem.obs_t_range[:-1]:
    # There seems to be some counting inconsistency?
    print "Adding noise to data at time t = {}".format(t)

    fr_obs.read(u, "obs", t)
    P_s.append(assemble( u**2 * dx))

    y[:] = random.randn(len(x)) * args.intensity
    P_n.append(assemble( v**2 * dx))

    x += y
    fw_obs_noisy.write(Function(u, name = "obs"), "obs", t)

# Signal-to-noise-ratio
import numpy
SNR = sum(P_s) / sum(P_n)
SNR_dB = 10 * numpy.log10(SNR)

print "\nFinished adding noise:"
print "----------------------"
print "Signal-to-noise ratio:  {0:1.4f} = {1:1.4f} dB".format(SNR, SNR_dB)
