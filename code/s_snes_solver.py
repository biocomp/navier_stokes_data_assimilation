from dolfin import *
from petsc4py import *
import numpy as np
import sys

sys.argv = [sys.argv[0]] + """
                        --petsc.sw_snes_monitor
                        --petsc.sw_snes_max_it 20
                        --petsc.sw_snes_converged_reason
                        --petsc.sw_snes_atol 1.0e-6
                        --petsc.sw_snes_rtol 1.0e-16

                        --petsc.sw_ksp_converged_reason
                        --petsc.sw_ksp_type fgmres
                        --petsc.sw_ksp_monitor
                        --petsc.sw_ksp_rtol 1.0e-10

                        --petsc.sw_pc_type fieldsplit
                        --petsc.sw_pc_fieldsplit_type schur
                        --petsc.sw_pc_fieldsplit_schur_factorization_type full
                        --petsc.sw_pc_fieldsplit_schur_precondition self

                        --petsc.sw_fieldsplit_0_ksp_type gmres
                        --petsc.sw_fieldsplit_0_ksp_rtol 1.0e-1
                        --petsc.sw_fieldsplit_0_pc_type hypre
                        --petsc.sw_fieldsplit_0_pc_hypre_type boomeramg

                        --petsc.sw_fieldsplit_1_ksp_type gmres
                        --petsc.sw_fieldsplit_1_ksp_rtol 1.0e-4
                        --petsc.sw_fieldsplit_1_pc_type hypre
                        --petsc.sw_fieldsplit_0_pc_hypre_type boomeramg

                        --petsc.sw_fieldsplit_1_inner_ksp_type gmres
                        --petsc.sw_fieldsplit_1_inner_ksp_rtol 1.0e-1
                        --petsc.sw_fieldsplit_1_inner_pc_type hypre
                        --petsc.sw_fieldsplit_0_pc_hypre_type boomeramg

                        --petsc.sw_fieldsplit_1_upper_ksp_type gmres
                        --petsc.sw_fieldsplit_1_upper_ksp_rtol 1.0e-4
                        --petsc.sw_fieldsplit_1_upper_pc_type hypre
                        --petsc.sw_fieldsplit_0_pc_hypre_type boomeramg

                        --petsc.sw_fieldsplit_0_ksp_converged_reason
                        --petsc.sw_fieldsplit_1_ksp_converged_reason
                        --petsc.sw_fieldsplit_1_inner_ksp_converged_reason
                        --petsc.sw_fieldsplit_1_upper_ksp_converged_reason
                        """.split()

# basic setup copied from the fenics stokes demo
mesh = UnitCubeMesh(8, 8, 8)
V = VectorFunctionSpace(mesh, 'CG', 2)
Q = FunctionSpace(mesh, 'CG', 1)
W = V * Q

def right(x, on_boundary):
    return x[0] > (1.0 - DOLFIN_EPS)
def left(x, on_boundary):
    return x[0] < DOLFIN_EPS
def top_bottom(x, on_boundary):
    return x[1] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS

noslip = Constant((0.0, 0.0, 0.0))
bc0 = DirichletBC(W.sub(0), noslip, top_bottom)
inflow = Expression(('-sin(x[1]*pi)', '0.0', '0.0'))
bc1 = DirichletBC(W.sub(0), inflow, right)
zero = Constant(0)
bc2 = DirichletBC(W.sub(1), zero, left)
bcs = [bc0, bc1, bc2]

z = Function(W)
(u, p) = z.split()
(v, q) = TestFunctions(W)

# Forms
F = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx

# Assemble preconditioner
u_trial, p_trial = TrialFunctions(W)
m = inner(grad(u_trial), grad(v))*dx + p_trial*q*dx
(M, _) = assemble_system(m, q*dx, bcs)

# export to petsc4py
M = as_backend_type(M).mat()

U = Function(W)

solver = PETScSNESSolver()
snes = solver.snes()
snes.setFromOptions()

# fieldsplit solve
ksp = snes.ksp
ksp.setType(PETSc.KSP.Type.TFQMR)
pc = ksp.getPC()
pc.setType(PETSc.PC.Type.FIELDSPLIT)
is0 = PETSc.IS().createGeneral(W.sub(0).dofmap().dofs())
is1 = PETSc.IS().createGeneral(W.sub(1).dofmap().dofs())

pc.setFieldSplitIS(('u', is0), ('p', is1))
pc.setFieldSplitType(0) # 0=additive

PETScOptions.set('snes_view')
PETScOptions.set('snes_monitor')
PETScOptions.set('ksp_monitor')

subksps = pc.getFieldSplitSubKSP()
subksps[0].setType("preonly")
subksps[0].getPC().setType("hypre")
subksps[1].setType("preonly")
subksps[1].getPC().setType("hypre")
#pc.setOperators(M, M)
ksp.setFromOptions()

class GeneralProblem(NonlinearProblem):
  def __init__(self, F, z, bcs, J=None):
    NonlinearProblem.__init__(self)
    self.fform = F
    self.z = z
    self.bcs = bcs
    if J is None:
      J = derivative(F, z)
    self.jacobian = J

  def F(self, b, x):
    assemble(self.fform, tensor=b)
    [bc.apply(b, x) for bc in self.bcs]

  def J(self, A, x):
    assemble(self.jacobian, tensor=A, keep_diagonal=True)
    [bc.apply(A) for bc in self.bcs]

problem = GeneralProblem(F, z, bcs=bcs, J=derivative(F, z))
solver.init(problem, z.vector())

zvec = as_backend_type(z.vector()).vec()
snes.solve(None, zvec)

print(np.max(np.abs(U.vector().array()-z.vector().array())))
