JOBFILES=(abel_jobs/generated/*.job)
for JOBFILE in "${JOBFILES[@]}"
do
    echo "sbatch ${JOBFILE}"
    sbatch $JOBFILE
done
