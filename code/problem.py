import argparse
from numpy import arange
from collections import OrderedDict
from dolfin import *
from dolfin_adjoint import *
from utils import FileReader, FileWriter, print0


class BaseProblem(object):


    def __init__(self, args):
        _args = self.default_parameters()
        _args.update(args.__dict__)
        self.args = argparse.Namespace(**_args)

    def default_parameters(self):
        return {}

    def __getattr__(self, atrrname):
        return getattr(self.args, atrrname)

    def __str__(self):
        s = "output dir {}\n".format(self.output_dir)
        s += "nu {}\n".format(self.nu)
        s += "theta {}\n".format(self.theta)
        s += "timestep {}\n".format(self.t_range[1] - self.t_range[0])
        s += "final time {}\n".format(self.t_range[-1])
        s += "time regularisation coefficient {}\n".format(self.beta)
        s += "space regularisation coefficient {}\n".format(self.alpha)
        s += "initial condition regularisation coefficient {}\n".format(self.gamma)
        s += "periodicity regularisation coefficient {}\n".format(self.periodicity_alpha)
        s += "Nitsche gamma coefficient {}\n".format(self.nitsche_gamma)
        s += "Optimisation maximimum iterations {}\n".format(self.opt_maxiter)
        s += "{} finite element pair\n".format(self.discretization)
        if self.time_average_observations:
            s += "time-averaged observations\n"
        else:
            s += "instantaneous observations\n"
        s += "observations\n"
        for i, (a, b) in enumerate(zip(self.obs_t_range[:-1], self.obs_t_range[1:])):
            s += "  observation {}: {} - {}\n".format(i, a, b)
        return s

    @property
    def velocity_space(self):
        return self._velocity_space

    @property
    def observation_space(self):
        return self._velocity_space

    @property
    def obs_measure(self):
        return dx

    def read_observations(self):
        obs = OrderedDict()
        fobs = FileReader("{}".format(self.observation_file))
        fobs_out = FileWriter("{}/obs".format(self.output_dir))

        for i in range(len(self.obs_t_range)-1):
            t0 = self.obs_t_range[i]
            t1 = self.obs_t_range[i+1]
            print0("Read observation for time range %f - %f (index %i)." % (t0, t1, i))
            obs[t0] = Function(self.observation_space, annotate=False, name="obs_{}".format(t0))

            # some renaming for paraview:
            obs[t0].rename("obs", obs[t0].label())
            fobs.read(obs[t0], "obs", t0)
            obs[t0].rename("obs_{}".format(t0), obs[t0].label())

            fobs_out.write(Function(obs[t0], name="obs"), "obs", t0)
        return obs

    def construct_controls(self):
        # Define the control function (inflow boundary condition)
        # We control the boundary condition at every time level
        print0("Set up control functions")
        g = OrderedDict()
        self.control_space = self.velocity_space #VectorFunctionSpace(self.mesh, 'CG', 2)
        t_old = self.t_range[0]
        for t in self.t_range[1:]:
            t_theta = self.theta*t + (1-self.theta)*t_old
            if self.dim == 3:
                g[t] = self.bc.bc(t_theta)
            else:
                self.bc.t = t_theta
                g[t] = interpolate(self.bc, self.control_space, name="Control_{}".format(t))

            t_old = t
        return g


##########################################################################
class DogBC(object):

    def __init__(self, V, facet_ids):
        self.u_bc_init = Function(V)
        bcs = [DirichletBC(V, 0.5*Constant((0, -0.4, -1)), facet_ids, 1),
               DirichletBC(V, 0.5*Constant((0, 0, -1)), facet_ids, 2),
               DirichletBC(V, Constant((0, 0.1, -1)), facet_ids, 3)]
        [bc.apply(self.u_bc_init.vector()) for bc in bcs]

    def bc(self, t):
        tfac = sin(pi*(1-t)**3)
        maxval = 1000
        bcfunc = self.u_bc_init.copy(deepcopy = True)
        bcfunc.rename("Control_{}".format(t), self.u_bc_init.label())
        vec = bcfunc.vector()
        vec *= tfac*maxval
        return bcfunc

class Dog3D(BaseProblem):

    def default_parameters(self):
        return dict(nu = 4.5,
                    theta = 1.0,
                    periodicity_alpha = 0.,
                    beta = 0.3,
                    alpha = 0.0,
                    convection = True)

    def __init__(self, args):

        super(Dog3D, self).__init__(args)

        import sys
        sys.setrecursionlimit(10000)

        self.dim = 3
        self.mesh_level = args.mesh_level

        # Facet file and boundary ids
        inflow_id = 3
        outflow_left_id = 2
        outflow_right_id = 1
        self.controlled_velocity_facet_ids = (inflow_id, outflow_left_id)
        self.controlled_pressure_facet_ids = ()
        self.wall_ids = [0]

        # Read the mesh
        mesh = Mesh()
        if "wide" in args.observation_file:
            mesh_file = "assimilation_data/meshes_wide/model.hdf5"
        else:
            mesh_file = "assimilation_data/meshes/model.hdf5"
        f =  HDF5File(mpi_comm_world(), mesh_file, "r")
        f.read(mesh, "Meshes/Mesh{}".format(self.mesh_level), False)
        self.mesh = mesh
        print "Mesh cells:    ", mesh.num_cells()
        print "Mesh facets:   ", mesh.num_facets()
        print "Mesh edges:    ", mesh.num_edges()
        print "Mesh vertices: ", mesh.num_vertices()
        print "="*20

        # Read facet function
        facet_mvc = MeshValueCollection("size_t", mesh, 2)
        f.read(facet_mvc,
                "Meshes/Mesh{}/MeshValueCollections/facet_domains".format(self.mesh_level))
        self.facet_ids = MeshFunction("size_t", mesh, facet_mvc)
        del f

        # Time parameters
        # the flow_rates.py script by JJ and Oyvind contains these time levels:
        # 0,0.0370,0.0740,0.1110,0.1480, 0.1850,0.2220,0.2590,0.2960,0.3330,0.3700,0.4070, 0.4440,0.4810,0.5180,0.5550,0.602
        # Note that all have the same timestep of 0.0370, except of the last one.
        # Assuming that this is an mistake, we will use 0.0370 consistently here.
        obs_dt = 0.0370
        T = 17 * obs_dt + DOLFIN_EPS
        self.obs_t_range = arange(0, T, obs_dt)

        self.t_range = arange(0, T, obs_dt/8)

        # Discretisation settings
        if self.discretization in ("p2p1", "th"):
            self._velocity_space = VectorFunctionSpace(mesh, "CG", 2)
        elif self.discretization == "p1p1":
            self._velocity_space = VectorFunctionSpace(mesh, "CG", 1)


        # Initial state
        self.u0 = interpolate(Constant((0, 0, 0)), self._velocity_space)

        # Initial guess of the controled inflow boundary
        self.bc = DogBC(self._velocity_space, self.facet_ids)

        # Switch on some optimisations
        parameters['form_compiler']['cpp_optimize_flags'] = "-Os -ffast-math -march=native"
        parameters['form_compiler']['cpp_optimize'] = True
        parameters['form_compiler']['optimize'] = False

    def __str__(self):
        s = "3D Dog Problem\n"
        s += "Mesh level {}\n".format(self.mesh_level)
        s += super(Dog3D, self).__str__()
        return s

    def read_observations(self):
        obs = OrderedDict()
        fobs = FileReader(self.observation_file)
        fobs_out = FileWriter("{}/obs".format(self.output_dir))

        for i, t in enumerate(self.obs_t_range[:-1]):
            print0("Read observation for time range %f - %f (index %i)." % (t,
                    self.obs_t_range[i+1], i))
            obs[t] = Function(self.observation_space, annotate=False, name="obs_{}".format(t))
            fobs.read(obs[t], "u150-ref{}{}".format(self.mesh_level, i))
            fobs_out.write(Function(obs[t], name="obs"), "obs", t)

        return obs


##########################################################################
class Bifurcation2D(BaseProblem):

    def default_parameters(self):
        return dict(nu = 30,
                    theta = .5,
                    periodicity_alpha = 0.,
                    beta  = 1e-6,
                    alpha = 1e-4,
                    convection = True)

    def __init__(self, args):

        super(Bifurcation2D, self).__init__(args)

        self.dim = 2
        # Facet file and boundary ids
        mesh_file = "mesh_bifurcation/mesh.hdf5"
        inflow_id = 1
        outflow_left_id = 2
        outflow_right_id = 3
        self.wall_ids = (10,)

        # Read the mesh
        f = HDF5File(mpi_comm_world(), mesh_file, 'r')
        mesh = Mesh()

        f.read(mesh, "Mesh", False)
        self.mesh = mesh

        # Read facet function
        facet_ids = FacetFunction("size_t", mesh)
        f.read(facet_ids, "Mesh/MeshFunction")
        del f
        self.facet_ids = facet_ids

        # Velocity space
        if self.discretization in ("p1p2", "th"):
            self._velocity_space = VectorFunctionSpace(mesh, "CG", 2)
        elif self.discretization == "p1p1":
            self._velocity_space = VectorFunctionSpace(mesh, "CG", 1)

        if args.mode == "generate":
            self.u0 = interpolate(Constant((0, 0)), self.velocity_space)
            d1 = assemble(1*ds(inflow_id, domain=mesh, subdomain_data=facet_ids))
            x1 = d1**-1 * assemble(Expression("x[0]")*ds(inflow_id, domain=mesh, subdomain_data=facet_ids))

            u1  = "1000*4*pow(d1,-2)*(x[0]-(x1-d1/2))*((x1+d1/2)-x[0])*(x[1]<0)"

            T = "sin(pi*pow(1-t,3))"

            bc_str = T+"*"+u1
            self.bc = Expression(("0", bc_str), t=0.,d1=d1,x1=x1)

            self.controlled_velocity_facet_ids = (inflow_id,)
            self.controlled_pressure_facet_ids = ()
        else:
            self.u0 = interpolate(Constant((0, 0)), self._velocity_space)
            self.bc = Expression(("0", "0"))

            self.controlled_velocity_facet_ids = (inflow_id, outflow_left_id)
            self.controlled_pressure_facet_ids = ()

        # Time parameters
        obs_dt = 0.0370
        T = 4 * obs_dt + DOLFIN_EPS
        self.obs_t_range = arange(0, T, obs_dt)
        self.t_range = arange(0, T, obs_dt/2)

    def __str__(self):
        s = "2D Bifurcation Problem\n"
        s += super(Bifurcation2D, self).__str__()
        return s


##########################################################################
class Aneurysm2D(BaseProblem):

    def default_parameters(self):
        return dict(nu = 5,
                    theta = .5,
                    periodicity_alpha = 0.,
                    beta = 1e-2,
                    alpha = 0.0,
                    convection = True)

    def __init__(self, args):

        super(Aneurysm2D, self).__init__(args)
        mode = self.mode

        self.dim = 2
        # Facet file and boundary ids
        mesh_file = "mesh_aneurysm/aneurysm_2D.hdf5"

        self.obs_domain_ids = (1,)

        if mode == "generate":
            inflow_id = 5
            outflow_left_id = 6
            outflow_right_id = 7
            self.wall_ids = [1]
            self.controlled_velocity_facet_ids = (inflow_id, outflow_right_id)
            self.controlled_pressure_facet_ids = ()

        else:
            inflow_id = 2
            outflow_left_id = 3
            outflow_right_id = 4
            self.wall_ids = [1]
            self.controlled_velocity_facet_ids = (inflow_id, outflow_left_id)
            self.controlled_pressure_facet_ids = ()

        self.physical_ds_ids = (2, 3, 4)
        self.ds_ids = (inflow_id, outflow_left_id, outflow_right_id)

        # Read the meshes
        f = HDF5File(mpi_comm_world(), mesh_file, 'r')
        mesh = Mesh()
        f.read(mesh, "Mesh", False)

        submesh_obs = Mesh()
        f.read(submesh_obs, "ObsMesh", False)

        # Not used at the moment
        #submesh_sim = Mesh()
        #f.read(submesh_sim, "SimMesh", False)

        self.mesh = mesh if mode == "generate" else submesh_obs

        self.facet_ids = facet_ids = FacetFunction("size_t", self.mesh)
        self.cell_ids = cell_ids = CellFunction("size_t", self.mesh)

        f.read(facet_ids, ("Mesh" if mode == "generate" else "ObsMesh") + "/FacetFunction")
        f.read(cell_ids, ("Mesh" if mode == "generate" else "ObsMesh") + "/CellFunction")

        f.close()

        self.ds = Measure("ds", domain=self.mesh, subdomain_data=self.facet_ids)

        # Time parameters
        obs_dt = 0.037
        T = 17 * obs_dt + DOLFIN_EPS
        self.obs_t_range = arange(0, T, obs_dt)
        self.t_range = arange(0, T, obs_dt/4)

        # Velocity space
        if self.discretization in ("p2p1", "th"):
            self._velocity_space = VectorFunctionSpace(self.mesh, "CG", 2)
        elif self.discretization == "p1p1":
            self._velocity_space = VectorFunctionSpace(self.mesh, "CG", 1)

        # Observation Space
        self._observation_space = VectorFunctionSpace(submesh_obs, "CG", 1)

        # Initial guess of the initial condition and the controlled inflow boundary
        if mode == "optimize":
            # The initial guess for the controls
            self.u0 = interpolate(Constant((0, 0)), self._velocity_space)
            self.bc = Expression(("0", "0"))
        else:
            self.u0 = interpolate(Constant((0, 0)), self._velocity_space)
            d1 = assemble(1*ds(inflow_id, domain=mesh, subdomain_data=facet_ids))
            d2 = assemble(1*ds(outflow_left_id, domain=mesh, subdomain_data=facet_ids))
            d3 = assemble(1*ds(outflow_right_id, domain=mesh, subdomain_data=facet_ids))
            x2 = d2**-1 * assemble(Expression("x[0]")*ds(outflow_left_id, domain=mesh, subdomain_data=facet_ids))
            x3 = d3**-1 * assemble(Expression("x[0]")*ds(outflow_right_id, domain=mesh, subdomain_data=facet_ids))
            u1  = "1000*4*pow(d1,-2)*x[0]*(d1-x[0])*(x[1]<0)"
            u2  = "380*4*pow(d2,-2)*(x[0]-(x2-d2/2))*((x2+d2/2)-x[0])*(x[1]>0)*(x[0]<-2)"
            u3  = "870*4*pow(d3,-2)*(x[0]-(x3-d3/2))*((x3+d3/2)-x[0])*(x[1]>0)*(x[0]>6)"
            T = "sin(pi*pow(1-t,3))"
            u_bc_str = "*".join((T,"+".join((u1,u2,u3)).join(("(",")"))))
            self.bc = Expression(("0.", u_bc_str), t=0.,d1=d1,d2=d2,d3=d3,x2=x2,x3=x3)

    def __str__(self):
        s = "2D Aneurysm problem\n"
        s += super(self.__class__, self).__str__()
        return s

    @property
    def observation_space(self):
        return self._observation_space

    @property
    def obs_measure(self):
        return Measure("dx", domain=self.mesh,
                       subdomain_id=self.obs_domain_ids,
                       subdomain_data=self.cell_ids)


def create_problem_from_commandline(mode):
    ############### Read the command line arguments #################
    parser = argparse.ArgumentParser()
    parser.add_argument('--problem', required=True, choices=["Bifurcation2D", "Aneurysm2D", "Dog3D"], help='name of the probem')
    parser.add_argument('--observation_file', help='base-filename (without extension) of hdf5 observations file')
    parser.add_argument('--output_dir', required=True, help='path where to store the observations (generate mode) or assimilated data (optimize mode)')
    parser.add_argument('--mesh_level', type=int, help='mesh level')
    parser.add_argument('--time_average_observations', action="store_true", help='assimilate observations that are averaged over time')
    parser.add_argument('--nu', type=float, help='viscosity')
    parser.add_argument('--nitsche_gamma', type=float, default=100, help='Nitsche Gamma parameter')
    parser.add_argument('--theta', type=float, help='Theta timestepping value')
    parser.add_argument('--periodicity_alpha', type=float, help='Periodicity parametrisation coefficient')
    parser.add_argument('--beta', type=float, help='Regularisation coefficient for the integral of dg/dt')
    parser.add_argument('--alpha', type=float, help='Regularisation coefficient on the integral of g')
    parser.add_argument('--gamma', type=float, default=0.0, help='Initial condition regularisation coefficient')
    parser.add_argument('--opt_maxiter', type=int, default=20, help='Maximum number of optimisation iterations')
    parser.add_argument('--opt_rjtol', type=float, default=1e-6, help='Relative stopping criterion for optimisation based on functional value decrease')
    parser.add_argument('--opt_rgtol', type=float, default=1e-3, help='Relative stopping criterion for optimisation based on gradient norm decrease')
    parser.add_argument('--opt_method', choices=["scipy", "ipopt", "tao", "moola"], default="scipy", help='Maximum number of optimisation iterations')
    parser.add_argument('--moola_norm', type=str, default="L2H1", help='Norm used by moola')
    parser.add_argument('--moola_norm_exact', action="store_true", help='use exact norm instead of approximation')
    parser.add_argument('--regularize_g', action="store_true", help='Apply regularization to g instead of u')
    parser.add_argument('--discretization', type = str.lower, choices = ["p1p1", "p2p1", "th"], default = "p1p1", help='choice of discretization')
    parser.add_argument('--lu_solver', type = str.lower, choices = ["mumps", "superlu_dist"], default = "mumps", help='choice of discretization')
    args = parser.parse_args()
    args.mode = mode

    # Handle special cases
    if args.mode == "generate" and not args.output_dir:
        parser.error('--output_dir must be set when --mode=generate.')
    if args.problem == "Dog3D" and args.mesh_level is None:
        parser.error('--mesh_level must be set when --problem=Dog3D.')

    # Delete arguments with value None. They will be replaced by default values
    for key in args.__dict__.keys():
        if getattr(args, key) == None: delattr(args, key)

    problems = {"Dog3D":         Dog3D,
                "Bifurcation2D": Bifurcation2D,
                "Aneurysm2D":    Aneurysm2D}
    return problems[args.problem](args)
