import moola
from collections import OrderedDict
#import pyipopt
from problem import *
from dolfin import *
from dolfin_adjoint import *
from ns_solver import NSSolver
from utils import ResultsWriter, print0
from linear_function import LinearFunction

parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(PROGRESS)

###################### Configure problem ############################
problem = create_problem_from_commandline(mode="optimize")
print0(problem)

###################### Setup controls ############################
g = problem.construct_controls()

###################### Run forward model ############################
# Run the model to annotate dolfin-adjoint's tape, generate the synthetic
# observations, and build the form for the objective functional
print0("Run forward model")
ns_solver = NSSolver(problem)
solver_params = {"nonlinear_method": "newton", "convection": problem.convection,
                 "annotate": True, "nu": problem.nu,
                 "nitsche_gamma": problem.nitsche_gamma, "theta": problem.theta,
                 "velocity_space": problem.velocity_space}

# An integrator to calculate the temporal averages of the velocity
integrator = LinearFunction(problem.t_range, [None]*len(problem.t_range))

u00 = problem.u0.copy(deepcopy = True)
u_values = []
for t, u, p in ns_solver.run(problem.t_range, problem.u0, g, **solver_params):
    u_values.append(Function(u, annotate = annotate))
    #u_values.append(project(u, u.function_space(), annotate = annotate))
    # Project the velocity state into the observation space
    u_t = project(u, problem.observation_space,
                  mesh=problem.observation_space.mesh(),
                  name="u_projected_{}".format(t),
                  solver_type="lu"
		)
    integrator.update(t, u_t)

#u_values.pop(0) # removing initial condition

# Print timings
set_log_level(INFO)
list_timings(TimingClear_keep, [TimingType_wall, TimingType_system])
set_log_level(ERROR)

###################### Construct objective functional #########################

# Read observations
obs = problem.read_observations()

# Construct the objective functional
dx = Measure("dx", domain=problem.mesh)
j = Constant(0)*dx

for i in range(len(problem.obs_t_range)-1):
    a, b = problem.obs_t_range[i], problem.obs_t_range[i+1]
    if problem.time_average_observations:
        # Compute time average
        model_obs = integrator.integrate(a, b)/(b-a)
    else:
        # Evaluate pointwise
        model_obs = integrator(a)

    # Add the difference to the objective functional
    j += 0.5*inner(model_obs - obs[a], model_obs - obs[a]) * problem.obs_measure

# Initial condition regularisation
j += Constant(0.5 * problem.gamma) * grad(problem.u0)**2*dx

# H1 regularisation over the boundary:
dsu_, dsp_ = ns_solver.dsu_, ns_solver.dsp_  # integration domain
dsg_ = dsu_ + dsp_

def get_function_space(ufl_obj):
    if hasattr(ufl_obj, "function_space"):
        return ufl_obj.function_space()
    elif hasattr(ufl_obj, "ufl_operands"):
        for op in ufl_obj.ufl_operands:
            F = get_function_space(op)
            if F: return F
    return None

def t_grad(w):
    """ Returns the tangential part of the gradient on cell facets."""
    W = get_function_space(w)
    if W.mesh().geometry().dim() == 1:
        return grad(w)
    else:
        n = FacetNormal(W.mesh())
        if len(w.ufl_shape) == 0:
            return grad(w) - n * inner(n, w)
        elif len(w.ufl_shape) == 1:
            return grad(w) - outer(grad(w) * n, n)


# not using this class at the moment
class IndicatorSubDomain(SubDomain):
    def __init__(self, indicator, tol = 0.5):
        SubDomain.__init__(self)
        self.indicator = indicator
        self.tol = tol

    def inside(self, x, on_boundary):
        return self.indicator(x) > self.tol

if problem.regularize_g:
    # only integrate
    h1_regularisation = sum([g0**2 + t_grad(g0)**2 for g0 in g.values()])
    dt = problem.t_range[1] - problem.t_range[0]
    j += Constant(0.5 * problem.alpha*dt) * h1_regularisation * dsg_

    # H1 regularisation in time
    time_regularisation = sum([(g1-g0)**2 +  t_grad(g1-g0)**2 for g1, g0 in zip(g.values()[:-1], g.values()[1:])])
    j += Constant(0.5 * problem.beta/dt) * time_regularisation * dsg_

else:
    h1_regularisation = sum([u_i**2 + t_grad(u_i)**2 for u_i in u_values])
    dt = problem.t_range[1] - problem.t_range[0]
    j += Constant(0.5 * problem.alpha*dt) * h1_regularisation * ds

    # H1 regularisation in time
    time_regularisation = sum([(u_i-u_j)**2 +  t_grad(u_i-u_j)**2 for u_i, u_j in zip(u_values[:-1], u_values[1:])])
    j += Constant(0.5 * problem.beta/dt) * time_regularisation * ds


# Enforce periodicity via quadratic penalty term
periodic_regularisation = (g.values()[0]-u)**2*dx
j += Constant(0.5 * problem.periodicity_alpha) * periodic_regularisation

# Scale functional
scale = 1
j = Constant(scale)*j
J = Functional(j)


###################### Optimisation setup ############################
parameters["adjoint"]["stop_annotating"] = True

# Define the dolfin-adjoint controls
c = [Control(problem.u0)]  # initial condition
c += [Control(gg) for gg in g.itervalues()]  # bc conditions

# Compute and plot the gradient (optional)
#dJ = compute_gradient(J, c, project=False, forget=False)
#for i, t in enumerate(problem.t_range):
#   plot(dJ[i], title="dJ, t = %f" % t)
#    interactive()

# Set up the optimization
def jhat_eval_pre(*args):
    print0("Running forward model...")
def jhat_eval_post(j, *args):
    print0(" j = {:.2e}".format(j))
def jhat_deriv(*args):
    print0("Running adjoint model")
def replay_cb(var, value, m):
    if "Control" in var.var.name:
        maxval = MPI.max(mpi_comm_world(), max(abs((value.vector().array()))))
        print0(" max({}) = {}".format(var.var.name, maxval))


# Pick a different index to Taylor test another control variable
i = 0

Jhat = ReducedFunctional(J, c[i], eval_cb_pre=jhat_eval_pre,
                               eval_cb_post=jhat_eval_post,
                               derivative_cb_pre=jhat_deriv,
                               replay_cb=replay_cb)

# Perform a sanity check. Test that the manually computed functional value
# is equal to the evaluation of the reduced functional for the same inputs
ctrl_vals = [u00] + g.values()
if problem.dim == 2:
    assert abs(Jhat(ctrl_vals[i]) - assemble(j)) < 1e-5/scale

    set_log_level(INFO)
    print "Min taylor convergence: ", Jhat.taylor_test(ctrl_vals[i])
