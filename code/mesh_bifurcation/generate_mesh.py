from dolfin import *

if not has_cgal():
    print "DOLFIN must be compiled with CGAL to run the mesh generator."
    exit(0)

# Create empty mesh
mesh = Mesh()

def generate_vessel(x0, x1, y0, y1, angle):
    ''' Generates a rotated recangle '''

    def rotate(p, angle):
        ''' Rotates a point around the origin '''

        return ((cos(angle) * p[0] - sin(angle) * p[1]),
                (sin(angle) * p[0] + cos(angle) * p[1]))

    # Create list of polygonal domain vertices
    # in counter-clockwise order
    domain_vertices = [(x0, y0),
                       (x1, y0),
                       (x1, y1),
                       (x0, y1)                      ]

    points = [Point(*rotate(p, angle)) for p in domain_vertices]
    polygon = Polygon(points)
    return polygon

v0 = generate_vessel(-2, 2, 0, 40, pi)
v1 = generate_vessel(-2, 2, 0, 40, pi/4)
v2 = generate_vessel(-2, 2, 0, 40, 7*pi/4)

circle = Circle(0, 10, 8)

domain = v0 + v1 + v2 + circle

mesh = Mesh(domain, 40)
File("aneurysm.xml") << mesh

plot(mesh, interactive=True)
