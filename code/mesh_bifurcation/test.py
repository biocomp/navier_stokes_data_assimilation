from mshr import *
from dolfin import *

left_vessel = Rectangle(dolfin.Point(-0.5, 10.), dolfin.Point(0.5, 20.))
left_vessel = CSGRotation(left_vessel, 10)

mesh = generate_mesh(left_vessel, 10)
