from math import sqrt, acos, pi

class VessellBifurcation(object):

    def __init__(self, r0=0.5, r1=0.5/sqrt(2)):
        self.r0 = r0
        self.r1 = r1
        self.r2 = (self.r0**3 - self.r1**3)**(1./3)

        self.phi1_radians = acos( (self.r0**4 + self.r1**4 - self.r2**4) / (2 *
            self.r0**2 * self.r1**2) )
        self.phi1_degree = self.phi1_radians * 360/(2*pi)

        self.phi2_radians = acos( (self.r0**4 - self.r1**4 + self.r2**4) / (2 *
            self.r0**2 * self.r2**2) )
        self.phi2_degree = self.phi2_radians * 360/(2*pi)

    def __str__(self):
        str = "r0 = {}\n".format(self.r0)
        str += "r1 = {}\n".format(self.r1)
        str += "r2 = {}\n".format(self.r2)
        str += "r0**3 - r1**3 + r2**3 = {}\n".format(self.r0**3 - self.r1**3 - self.r2**3)
        str += "phi1 = {}\n".format(self.phi1_degree)
        str += "phi2 = {}\n".format(self.phi2_degree)
        return str

    def gmsh_parameters(self):
        f = open('parameters.geo', 'w')
        parameters = "r0 = {};\n".format(self.r0)
        parameters += "r1 = {};\n".format(self.r1)
        parameters += "r2 = {};\n".format(self.r2)
        parameters += "phi1_degree = {};\n".format(self.phi1_degree)
        parameters += "phi2_degree = {};\n".format(self.phi2_degree)
        parameters += "phi1_radians = {};\n".format(self.phi1_radians)
        parameters += "phi2_radians = {};\n".format(self.phi2_radians)
        f.write(parameters)
        f.close()



if __name__ == "__main__":
    bi = VessellBifurcation()
    print bi
    bi.gmsh_parameters()
