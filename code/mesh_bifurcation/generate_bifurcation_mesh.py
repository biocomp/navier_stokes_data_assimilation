from mshr import *
from dolfin import *
from bifurcation import VessellBifurcation

param = VessellBifurcation() # The blood vessel parameters
                             # derived from Murray's law
L = 3.    # Length of blood vessels
res = 45  # Mesh resolution.

vessel0 = Rectangle(Point(-param.r0, -L), Point(param.r0, L/10))

vessel1 = Rectangle(Point(-param.r1, 0), Point(param.r1, L))
vessel1 = CSGRotation(vessel1, Point(0, 0), param.phi1_radians)

vessel2 = Rectangle(Point(-param.r2, 0), Point(param.r2, L))
vessel2 = CSGRotation(vessel2, Point(0, 0), -param.phi2_radians)

vessel = vessel0 + vessel1 + vessel2
mesh = generate_mesh(vessel, res)

class Boundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class Vessel0(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        return on_boundary and abs(x[1]+L) < tol

class Vessel1(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        angle = pi - param.phi1_radians
        y_rot = x[0]*sin(angle) + x[1]*cos(angle)
        return abs(y_rot+L) < tol

class Vessel2(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        angle = pi + param.phi2_radians
        y_rot = x[0]*sin(angle) + x[1]*cos(angle)
        return abs(y_rot+L) < tol

boundary = Vessel1()
subdomains = MeshFunction('size_t', mesh, 1)
subdomains.set_all(0)
Boundary().mark(subdomains, 10)
Vessel0().mark(subdomains, 1)
Vessel1().mark(subdomains, 2)
Vessel2().mark(subdomains, 3)


f = HDF5File(mpi_comm_world(), "mesh.hdf5", 'w')
f.write(mesh, "Mesh")
f.write(subdomains, "Mesh/MeshFunction")

plot(subdomains, interactive=True, title="Subdomains")
