// Generated with bifurcation.py
Include "parameters.geo";

l0 = 2.0;
l1 = 2.0;
l2 = 2.0;
n = 1.0;

// Vessel 0
Point(1) = {-r0, 0, 0, n};
Point(2) = { r0, 0, 0, n};
Point(3) = { r0, -l0, 0, n};
Point(4) = {-r0, -l0, 0, n};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

// Vessel 1
Point(10) = {-r1, 0, 0, n};
Point(20) = { r1, 0, 0, n};
Point(30) = { r1, -l1, 0, n};
Point(40) = {-r1, -l1, 0, n};
Line(10) = {10, 20};
Line(20) = {20, 30};
Line(30) = {30, 40};
Line(40) = {40, 10};
Line Loop(50) = {10, 20, 30, 40};
Plane Surface(60) = {50};

Rotate {{0, 0, 1}, {0, 0, 0}, Pi-phi1_radians} {
  Surface{60};
}

// Vessel 2
Point(100) = {-r2, 0, 0, n};
Point(200) = { r2, 0, 0, n};
Point(300) = { r2, -l2, 0, n};
Point(400) = {-r2, -l2, 0, n};
Line(100) = {100, 200};
Line(200) = {200, 300};
Line(300) = {300, 400};
Line(400) = {400, 100};
Line Loop(500) = {100, 200, 300, 400};
Plane Surface(600) = {500};

Rotate {{0, 0, 1}, {0, 0, 0}, Pi+phi2_radians} {
  Surface{600};
}
