"""This demo solves one step of the time-dependent stokes equations,
using a PETSc Krylov solver and a relatively sophisticated
preconditioner making use of the PETSc fieldsplit functionality. The
preconditioner is designed to perform well over wide ranges of
viscosity parameters mu and time-stepping lengths dt. This property
makes this demo useful for validating the solver implementation.

"""
import petsc4py
import mpi4py
import dolfin
from dolfin import *
from petsc4py import PETSc
import numpy
from time import time
import sys
import argparse

from petsc_util import *

#################
# parsing
#################
parser = argparse.ArgumentParser(description = "a Uniform Stokes solver")
parser.add_argument("--N", required = False, type = int, default = 32, help = "a mesh parameter")
parser.add_argument("--mu", required = False, type = float, default = 1.0, help = "the viscosity")
parser.add_argument("--dt", required = False, type = float, default = 1.0, help = "time stepping parameter")

parsed_args = parser.parse_args()
if rank() == 0:
    info("N = {0:3d} : mu = {1:1.2e} : dt = {2:1.2e}\n".format(parsed_args.N, parsed_args.mu, parsed_args.dt))


###################################
# Problem formulation and assembly
###################################
N = parsed_args.N
mesh = UnitSquareMesh(N,N)

# Parameters:
dt = parsed_args.dt
mu = parsed_args.mu 

walls = CompiledSubDomain("on_boundary&&((x[1]<DOLFIN_EPS)||(x[1]>1-DOLFIN_EPS))")
inlet = CompiledSubDomain("on_boundary&&(x[0]<DOLFIN_EPS)")
outlet = CompiledSubDomain("on_boundary&&(x[0]>1-DOLFIN_EPS)")


V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)
W = V * Q

f = Function(V)
u, p = TrialFunctions(W)
v, q = TestFunctions(W)

a0 = Constant(1./dt) * inner(u, v) * dx \
   + Constant(mu) * inner(grad(u), grad(v)) * dx
d = q * div(u) * dx
g = p * div(v) * dx

a = a0 + d + g
m = Constant(1.) * inner(u, v) * dx + Constant(1.) *  p * q * dx
r = Constant(1.)*inner(grad(p), grad(q))*dx 
l = inner(f, v) * dx

bcs = [DirichletBC(W.sub(0), Expression(("4*x[1]*(1-x[1])","0")), inlet),
       DirichletBC(W.sub(1), Constant(0.), outlet),
       DirichletBC(W.sub(0), Constant((0.0,0.0)), walls)]

PETSc.COMM_WORLD.barrier()
T = time()
A, b = assemble_system(a, l, bcs = bcs)
M, _ = assemble_system(m, l, bcs = bcs)
R, _ = assemble_system(r, l, bcs = bcs)
PETSc.COMM_WORLD.barrier()
if rank() == 0: info("Assembly completed in %.2f s\n"%(time()-T))

#########################
# Solving with PETSc KSP
#########################
T = time()
solver_options = dict(#help = "",
                      stokes_ksp_type = "minres",
                      #stokes_ksp_monitor = "",
                      stokes_ksp_converged_reason = "",
                      stokes_ksp_rtol = 1e-6,
                      stokes_ksp_max_it = 100,
                      #stokes_ksp_initial_guess_nonzero = "",
                      stokes_pc_type = "fieldsplit",
                      stokes_pc_fieldsplit_type = "additive",
                      stokes_fieldsplit_0_ksp_type = "preonly",
                      stokes_fieldsplit_0_pc_type = "mat",
                      stokes_fieldsplit_1_ksp_type = "preonly",
                      stokes_fieldsplit_1_pc_type = "mat")
                     
map(PETSc.Options().setValue, solver_options.keys(), solver_options.values())

solver = PETScKrylovSolver()
solver.ksp().setOptionsPrefix("stokes_")

solver.ksp().setFromOptions()
solver.ksp().setOperators(A = mat(A), P = mat(M)) # Needs to be set before fieldsplit

# Set up field split
index_sets = [PETSc.IS().createGeneral(W.sub(i).dofmap().dofs(), comm = PETSc.COMM_WORLD) for i in (0,1)]
solver.ksp().pc.setFieldSplitIS(*[(str(i), index_sets[i]) for i in xrange(2)])
solver.ksp().setUp()

PETSc.COMM_WORLD.barrier()
T = time()
A00 = mat(A).getSubMatrix(index_sets[0],index_sets[0])
Mp  = mat(M).getSubMatrix(index_sets[1],index_sets[1])
Ap  = mat(R).getSubMatrix(index_sets[1],index_sets[1])
PETSc.COMM_WORLD.barrier()
if rank() == 0: info("Submatrices extracted in %.2f s\n"%(time()-T))

ksp0, ksp1 = solver.ksp().pc.getFieldSplitSubKSP()

# Preconditioning the velocity blocks
P0 = AMG(A00)
ksp0.setOperators(A = P0, P = P0)
ksp0.setUp()

# preconditioning the pressure blocks
P1a = AMG(Ap)
P1b = SOR(Mp)
P1 = (1./dt) * P1a + mu * P1b
ksp1.setOperators(A = P1, P = P1)
ksp1.setUp()

wh = Function(W); uh, ph = wh.split()
x = wh.vector()

T = time()
solver.ksp().solve(vec(b), vec(x))
PETSc.COMM_WORLD.barrier()

if rank() == 0: info("Solver finished in %.2f s\n"%(time()-T))
