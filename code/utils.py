from fenics import mpi_comm_world, XDMFFile, HDF5File, MPI, mpi_comm_world
from dolfin_adjoint import project, Function


class FileWriter(object):
    def __init__(self, base_file, save_as=["hdf5", "xdmf"]):

        self.save_as = save_as

        if "hdf5" in save_as:
            self.hdf5 = HDF5File(mpi_comm_world(), "{}.hdf5".format(base_file), "w")

        if "xdmf" in save_as:
            self.xdmf =  XDMFFile(mpi_comm_world(), "{}.xdmf".format(base_file))
            self.xdmf.parameters["flush_output"] = True
            self.xdmf.parameters["rewrite_function_mesh"] = False

    def write(self, f, name, t):

        if "hdf5" in self.save_as:
            self.hdf5.write(f, "{}/{}".format(name, float(t)))
            self.hdf5.flush()

        if "xdmf" in self.save_as:
            self.xdmf.write(f, t)


class FileReader(object):
    def __init__(self, base_file):

        self.hdf5 = HDF5File(mpi_comm_world(), "{}.hdf5".format(base_file), "r")

    def read(self, f, name, t=None):

        if t is not None:
            self.hdf5.read(f, "{}/{}".format(name, float(t)))
        else:
            self.hdf5.read(f, "{}".format(name))

class ResultsWriter(object):
    def __init__(self, t_range, output_dir, obs_filename="obs_assimilated"):

        # Create output files for state, controls and observations
        self.fu = FileWriter("{}/u".format(output_dir))
        self.fp = FileWriter("{}/p".format(output_dir))
        self.fg = FileWriter("{}/g".format(output_dir))
        self.fobs = FileWriter("{}/{}".format(output_dir, obs_filename))

        self.t_range = t_range
        self.g_tmp = None

    def write_state(self, t, u, p, g):
        # Write observations
        print0("Write solution at time level %f." % t)
        self.fu.write(u, "u", t)
        self.fp.write(p, "p", t)


        if t != self.t_range[0]:

            if self.g_tmp is None:
                g_tmp = g.itervalues().next().copy(deepcopy = True)

            g_tmp.assign(g[t], annotate=False)
            self.fg.write(g_tmp, "g", t)

    def write_observations(self, integrator, obs_t_range, averaged=False,
                           noise = None, init = 0,):

        # Write observations
        V = integrator.yy[0].function_space()
        obs = Function(V, name="obs")

        for i, y in enumerate(integrator.yy):
            integrator.yy[i] = y.vector()

        for i in range(len(obs_t_range)-1):
            a, b = obs_t_range[i], obs_t_range[i+1]
            print0("Writing observation from averaged interval ({}, {}).".format(a,b))
            if averaged:
                obs.vector()[:] = integrator.integrate(a, b)/(b-a)
            else:
                obs.vector()[:] = integrator(a)
            if noise not in (0, None):
                from numpy.random import randn, seed
                seed(init)
                obs.vector()[:] += noise * randn(len(obs.vector()))
            self.fobs.write(obs, "obs", a)

def print0(s):
    if MPI.rank(mpi_comm_world()) == 0:
        print s
