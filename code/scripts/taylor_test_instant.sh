#!/bin/bash
# NOISE=0 must be defined in the shell
NU=3.5
ALPHA=1e-5
BETA=1e-5
GAMMA=1e-5
PROBLEM=Aneurysm2D
MAXITER=100
RJTOL=1e-4
OBS=results_aneurysm/instant/obs
OPTMETHOD=moola
NORM=H1H1
DISCRETIZATION=p1p1

OUT=results_taylor_test/instant/assimilated_${NORM}_${NOISE}_noise
OBSFILE=$OBS/obs_${NOISE}_noise
echo "Observation file: ${OBSFILE}"
python taylor_test.py --discretization=$DISCRETIZATION --problem $PROBLEM --observation_file $OBSFILE --output_dir $OUT --opt_maxiter $MAXITER --alpha=$ALPHA --beta=$BETA --gamma=$GAMMA --opt_method=$OPTMETHOD --moola_norm=$NORM --nu=$NU --opt_rjtol=$RJTOL 2>&1 
