#!/bin/bash
declare -a NOISES=(180 255 360)
NU=3.5
PROBLEM=Aneurysm2D
OBSOUT=results_aneurysm/instant/obs
DISCRETIZATION=p1p1

python run_model.py --problem $PROBLEM --output_dir $OBSOUT --nu=$NU --discretization=$DISCRETIZATION

cp $OBSOUT/obs.h5 $OBSOUT/obs_0_noise.h5
cp $OBSOUT/obs.hdf5 $OBSOUT/obs_0_noise.hdf5

for NOISE in "${NOISES[@]}"
do
    python add_noise.py --problem $PROBLEM --infile $OBSOUT/obs --outfile $OBSOUT/obs_${NOISE}_noise --intensity $NOISE
done
