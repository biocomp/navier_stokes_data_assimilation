#!/bin/bash
NU=3.5
ALPHA=1e-5
BETA=1e-5
GAMMA=1e-5
PROBLEM=Dog3D
MAXITER=100
RJTOL=1e-4
LEVEL=3
OPTMETHOD=moola
NORM=H1H1
DISCRETIZATION=p1p1

OUT=results_dog/averaged/assimilated_${NORM}_${LEVEL}_level
OBSFILE=assimilation_data/VENC150_wide/u150-ref$LEVEL/u150-ref$LEVEL
LOG=${OUT}.log
echo "Observation file: ${OBSFILE}"
mpirun python assimilate_data.py --discretization=$DISCRETIZATION --time_average_observations --problem $PROBLEM --mesh_level=$LEVEL --observation_file $OBSFILE --output_dir $OUT --opt_maxiter $MAXITER --alpha=$ALPHA --beta=$BETA --gamma=$GAMMA --opt_method=$OPTMETHOD --moola_norm=$NORM --nu=$NU --opt_rjtol=$RJTOL 2>&1 | tee $LOG
