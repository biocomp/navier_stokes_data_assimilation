gmsh -2 aneurysm_2D.geo
dolfin-convert aneurysm_2D.msh aneurysm_2D.xml
python xml2hdf5b.py aneurysm_2D.xml
python extract_submesh.py --file aneurysm_2D.hdf5 --name ObsMesh --ids "1"
python extract_submesh.py --file aneurysm_2D.hdf5 --name SimMesh --ids "1 3"
