from dolfin import *
import sys

file_name = sys.argv[1].split(".")[0]
f = HDF5File(mpi_comm_world(), file_name + ".hdf5", 'w')

# Read mesh and mesh function
mesh = Mesh(file_name + ".xml")
facet_domains = MeshFunction('size_t', mesh, file_name + "_facet_region.xml")
cell_domains = MeshFunction('size_t', mesh, file_name + "_physical_region.xml")

f.write(mesh, "Mesh")
f.write(facet_domains, "Mesh/FacetFunction")
f.write(cell_domains, "Mesh/CellFunction")

f.close()
