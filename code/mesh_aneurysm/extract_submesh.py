from dolfin import *
import sys
import argparse

print "Warning : Only works in serial"

### Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--file', required=True,
        help='name of input file')
parser.add_argument('--ids', required=True,
        help='indicies defining the subdomain')
parser.add_argument('--name', required=True,
        help='name of submesh')
args = parser.parse_args()

subdomain_ids = map(int, args.ids.split(" "))

def restrict_facet_function(ff1, mesh2):
    parameters["reorder_dofs_serial"] = False
    parameters["allow_extrapolation"] = True
    mesh1 = ff1.mesh()
    V1 = FunctionSpace(mesh1, "CR", 1)
    V2 = FunctionSpace(mesh2, "CR", 1)

    v1 = Function(V1)

    v1.vector()[:] = ff1.array()
    v2 = interpolate(v1, V2)

    ff2 = FacetFunction("size_t", mesh2)
    ff2.set_values(v2.vector().array().round().astype("uintp"))

    parameters["reorder_dofs_serial"] = True
    parameters["allow_extrapolation"] = False
    return ff2


def restrict_cell_function(cf1, mesh2):
    parameters["reorder_dofs_serial"] = False
    parameters["allow_extrapolation"] = True
    mesh1 = cf1.mesh()
    V1 = FunctionSpace(mesh1, "DG", 0)
    V2 = FunctionSpace(mesh2, "DG", 0)

    v1 = Function(V1)

    v1.vector()[:] = cf1.array()
    v2 = interpolate(v1, V2)

    cf2 = CellFunction("size_t", mesh2)
    cf2.set_values(v2.vector().array().round().astype("uintp"))

    parameters["reorder_dofs_serial"] = True
    parameters["allow_extrapolation"] = False
    return cf2


file_name = args.file.split(".")[0]
f = HDF5File(mpi_comm_world(), file_name + ".hdf5", 'r')

# Read the mesh
mesh = Mesh()
f.read(mesh, "Mesh", False)

# Read facet function
facet_ids = FacetFunction("size_t", mesh)
f.read(facet_ids, "Mesh/FacetFunction")

# Read cell function
cell_ids = CellFunction("size_t", mesh)
f.read(cell_ids, "Mesh/CellFunction")
f.close()

# Get submesh
if len(subdomain_ids) == 1:
    submesh = SubMesh(mesh, cell_ids, subdomain_ids[0])
else:
    tmp_cd = CellFunction("size_t", mesh)
    tmp_cd.array()[:] = map(lambda i: int(i in subdomain_ids), cell_ids)
    submesh = SubMesh(mesh, tmp_cd, 1)
    del tmp_cd


# restrict facet function
sub_facet_ids = restrict_facet_function(facet_ids, submesh)
sub_cell_ids = restrict_cell_function(cell_ids, submesh)

# Write submesh and subdomains
f = HDF5File(mpi_comm_world(), file_name + ".hdf5", 'a')
f.write(submesh, args.name)
f.write(sub_facet_ids, args.name + "/FacetFunction")
f.write(sub_cell_ids, args.name + "/CellFunction")

f.close()
