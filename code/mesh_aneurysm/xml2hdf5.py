from dolfin import *

f = HDF5File(mpi_comm_world(), "mesh.hdf5", 'w')

# Read mesh and mesh function
mesh = Mesh("mesh.xml")
mesh_function = MeshFunction('size_t', mesh, "mesh_facet_region.xml")

f.write(mesh, "Mesh")
f.write(mesh_function, "Mesh/MeshFunction")

del f
