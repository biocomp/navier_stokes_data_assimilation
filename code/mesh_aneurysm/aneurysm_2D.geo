s = 3.0;
t = s;
L = 2;
h = 0.045 * s; // mesh size

// points defining observed domain
Point(1)  = { 0.0    ,  0.0    , 0, h};
Point(2)  = { 1.0 * s,  0.0 * t, 0, h};
Point(3)  = {-0.1 * s,  1.7 * t, 0, h};
Point(4)  = {-1.7 * s,  4.0 * t, 0, h};
Point(5)  = {-1.7 * s,  5.0 * t, 0, h};
Point(6)  = { 0.9 * s,  2.1 * t, 0, h};
Point(7)  = { 1.8 * s,  3.0 * t, 0, h};
Point(8)  = { 2.7 * s,  4.0 * t, 0, h};
Point(9)  = {-0.8 * s,  5.0 * t, 0, h};
Point(10) = {-0.8 * s,  4.0 * t, 0, h};
Point(11) = { 0.1 * s,  2.6 * t, 0, h};
Point(12) = { 0.7 * s,  2.8 * t, 0, h};
Point(13) = { 1.7 * s,  3.9 * t, 0, h};
Point(14) = { 2.1 * s,  4.6 * t, 0, h};
Point(15) = {-0.5 * s,  4.0 * t, 0, h};
Point(16) = { 1.5 * s,  4.0 * t, 0, h};
Point(17) = { 0.5 * s,  4.5 * t, 0, h};

// points defining extension domain
Point(18) = { 0.0    , -L * t, 0, h};
Point(19) = { 1.0 * s, -L * t, 0, h};

//Point(20) = { 4.1 * s,  5.4 * t, 0, h};
//Point(21) = { 3.5 * s,  6.0 * t, 0, h};
Point(20) = { 2.95 * s,  4.6 * t, 0, h};
Point(21) = { 2.95 * s,  (4.6 + L) * t, 0, h};
Point(22) = { 2.1  * s,  (4.6 + L) * t, 0, h};

Point(23)  = {-0.8 * s,  (5.0 + L) * t, 0, h};
Point(24)  = {-1.7 * s,  (5.0 + L) * t, 0, h};


// lines and surfaces for the obs domain
BSpline(1) = {1, 3, 4, 5};
BSpline(2) = {2, 6, 7, 8};
BSpline(3) = {9, 10, 11, 11, 15, 17, 16, 12, 12, 13, 14};

Line(4) = {1, 2};
Line(5) = {5, 9};
Line(6) = {14, 8};

Line Loop(1) = {4, 2,-6, -3, -5, -1};
Plane Surface(1) = {1};



// line and surfaces for the ext domain
Line(7) = {1,  18};
Line(8) = {18, 19};
Line(9) = {19,  2};

Circle(10) = {8, 14, 20};
Line(11) = {20, 21};
Line(12) = {21, 22};
Line(13) = {22, 14};

Line(14) = { 9, 23};
Line(15) = {23, 24};
Line(16) = {24,  5};

Line Loop(2) = {7, 8, 9, -4};
Line Loop(3) = {14, 15, 16, 5};
Line Loop(4) = {10, 11, 12, 13, 6};



// physical domains
Physical Line(1) = {1, 2, 3, 7, 9, 10, 11, 13, 14, 16}; // Walls
Physical Line(2) = {4}; // inflow
Physical Line(3) = {5}; // left out
Physical Line(4) = {6}; // right out
Physical Line(5) = {8}; // ext_in
Physical Line(6) = {15}; // ext_left
Physical Line(7) = {12}; // ext_right

Physical Surface(1) = {1};

Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Physical Surface(2) = {2};
Physical Surface(3) = {3};
Physical Surface(4) = {4};
