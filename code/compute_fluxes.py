import sys
import argparse
from dolfin import *
from problem import *
from utils import FileReader, FileWriter, ResultsWriter, print0

#parameters["allow_extrapolation"] = True
set_log_level(ERROR)

############### Read the command line arguments #################
parser = argparse.ArgumentParser()
parser.add_argument('--dim', required=True, type=int,
        help='dimension (must be 2 or 3)')
parser.add_argument('--dir', required=True,
        help='where to read files')


args = parser.parse_args()

if args.dim == 3:
    raise NotImplementedError()

if args.dim == 2:
    problem = TestCase2D("generate")
    meshfile = "mesh_2d/Testcase2D.hdf5"

    mesh = problem.observation_space.mesh()
    fd = FacetFunction("size_t", mesh)
    f = HDF5File(mpi_comm_world(), meshfile, "r")
    f.read(fd, "ObsMesh/FacetFunction")

u = Function(problem.observation_space)
n = FacetNormal(mesh)
_ds = Measure("ds", domain = mesh, subdomain_data = fd)

data = {}
file_names = ["obs", "obs_noisy", "obs_assimilated"]
for file_name in file_names:
    try:
        f = HDF5File(mpi_comm_world(), args.dir + "/" + file_name + ".hdf5", "r")
        data[file_name] = numpy.zeros((len(problem.physical_ds_ids), len(problem.obs_t_range) - 1))
    except:
        continue
    for i,t in enumerate(problem.obs_t_range[:-1]):
        fn = file_name if file_name != "obs_assimilated" else "obs"
        f.read(u, fn + "/" + str(t))
        fluxes = [assemble(inner(u, -n) * _ds(j)) for j in problem.physical_ds_ids]
        data[file_name][:,i] = fluxes


from matplotlib import pyplot
from IPython import embed; embed()
