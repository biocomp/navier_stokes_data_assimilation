export NU=3.5
export DISCRETIZATION=p1p1
echo "Assuming nu=$NU"
echo "Assuming discretization=$DISCRETIZATION"
find . -type d -name "assimilated_*" -exec bash -c 'echo {} && python compute_metrics.py --problem Aneurysm2D --u_assim {}/u --u_true {}/../obs/u --outfile {}/diff --statdir {} --logfile {}/`basename {}`.log --nu=$NU --discretization=$DISCRETIZATION' \;
