from collections import OrderedDict
from dolfin import *
from problem import create_problem_from_commandline
from ns_solver import NSSolver
from utils import ResultsWriter, print0
from linear_function import LinearFunction

parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(PROGRESS)

###################### Configure problem ############################
problem = create_problem_from_commandline("generate")
print0(problem)

###################### Setup controls ############################
g = problem.construct_controls()

###################### Run forward model ############################
# Run the model to annotate dolfin-adjoint's tape, generate the synthetic
# observations, and build the form for the objective functional
print0("Run forward model")
ns_solver = NSSolver(problem)
solver_params = {"nonlinear_method": "newton", "convection": problem.convection,
                 "annotate": True, "nu": problem.nu,
                 "nitsche_gamma": problem.nitsche_gamma, "theta": problem.theta,
                 "velocity_space": problem.velocity_space}

# An integrator to calculate the temporal averages of the velocity
integrator = LinearFunction(problem.t_range, [None]*len(problem.t_range))

results_writer = ResultsWriter(problem.t_range, problem.output_dir, obs_filename="obs")

for t, gg in g.iteritems():
    maxval = MPI.max(mpi_comm_world(), max(abs((gg.vector().array()))))
    print0(" max(g[t={}]) = {}".format(t, maxval))

u00 = problem.u0.copy(deepcopy = True)
for t, u, p in ns_solver.run(problem.t_range, problem.u0, g, **solver_params):

    # Project the velocity state into the observation space
    u.set_allow_extrapolation(True)
    u_t = project(u, problem.observation_space)
    integrator.update(t, u_t)

    results_writer.write_state(t, u, p, g)

# Print timings
set_log_level(INFO)
list_timings(TimingClear_keep, [TimingType_wall, TimingType_system])
set_log_level(ERROR)

results_writer.write_observations(integrator, problem.obs_t_range, problem.time_average_observations)
