import moola
from collections import OrderedDict
#import pyipopt
from problem import *
from dolfin import *
from dolfin_adjoint import *
from ns_solver import NSSolver
from utils import ResultsWriter, print0
from linear_function import LinearFunction

parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(PROGRESS)

###################### Configure problem ############################
problem = create_problem_from_commandline(mode="optimize")
print0(problem)

###################### Setup controls ############################
g = problem.construct_controls()

###################### Run forward model ############################
# Run the model to annotate dolfin-adjoint's tape, generate the synthetic
# observations, and build the form for the objective functional
print0("Run forward model")
ns_solver = NSSolver(problem)
solver_params = {"nonlinear_method": "newton", "convection": problem.convection,
                 "annotate": True, "nu": problem.nu,
                 "nitsche_gamma": problem.nitsche_gamma, "theta": problem.theta,
                 "velocity_space": problem.velocity_space}

# An integrator to calculate the temporal averages of the velocity
integrator = LinearFunction(problem.t_range, [None]*len(problem.t_range))

u00 = problem.u0.copy(deepcopy = True)
u_values = []
for t, u, p in ns_solver.run(problem.t_range, problem.u0, g, **solver_params):
    u_values.append(Function(u, annotate = annotate))
    #u_values.append(project(u, u.function_space(), annotate = annotate))
    # Project the velocity state into the observation space
    u_t = project(u, problem.observation_space,
                  mesh=problem.observation_space.mesh(),
                  name="u_projected_{}".format(t),
                  solver_type="lu"
		)
    integrator.update(t, u_t)

#u_values.pop(0) # removing initial condition

# Print timings
set_log_level(INFO)
list_timings(TimingClear_keep, [TimingType_wall, TimingType_system])
set_log_level(ERROR)

###################### Construct objective functional #########################

# Read observations
obs = problem.read_observations()

# Construct the objective functional
dx = Measure("dx", domain=problem.mesh)
j = Constant(0)*dx

for i in range(len(problem.obs_t_range)-1):
    a, b = problem.obs_t_range[i], problem.obs_t_range[i+1]
    if problem.time_average_observations:
        # Compute time average
        model_obs = integrator.integrate(a, b)/(b-a)
    else:
        # Evaluate pointwise
        model_obs = integrator(a)

    # Add the difference to the objective functional
    j += 0.5*inner(model_obs - obs[a], model_obs - obs[a]) * problem.obs_measure

# Initial condition regularisation
j += Constant(0.5 * problem.gamma) * grad(problem.u0)**2*dx

# H1 regularisation over the boundary:
dsu_, dsp_ = ns_solver.dsu_, ns_solver.dsp_  # integration domain
dsg_ = dsu_ + dsp_

def get_function_space(ufl_obj):
    if hasattr(ufl_obj, "function_space"):
        return ufl_obj.function_space()
    elif hasattr(ufl_obj, "ufl_operands"):
        for op in ufl_obj.ufl_operands:
            F = get_function_space(op)
            if F: return F
    return None

def t_grad(w):
    """ Returns the tangential part of the gradient on cell facets."""
    W = get_function_space(w)
    if W.mesh().geometry().dim() == 1:
        return grad(w)
    else:
        n = FacetNormal(W.mesh())
        if len(w.ufl_shape) == 0:
            return grad(w) - n * inner(n, w)
        elif len(w.ufl_shape) == 1:
            return grad(w) - outer(grad(w) * n, n)


# not using this class at the moment
class IndicatorSubDomain(SubDomain):
    def __init__(self, indicator, tol = 0.5):
        SubDomain.__init__(self)
        self.indicator = indicator
        self.tol = tol

    def inside(self, x, on_boundary):
        return self.indicator(x) > self.tol

if problem.regularize_g:
    # only integrate
    h1_regularisation = sum([g0**2 + t_grad(g0)**2 for g0 in g.values()])
    dt = problem.t_range[1] - problem.t_range[0]
    j += Constant(0.5 * problem.alpha*dt) * h1_regularisation * dsg_

    # H1 regularisation in time
    time_regularisation = sum([(g1-g0)**2 +  t_grad(g1-g0)**2 for g1, g0 in zip(g.values()[:-1], g.values()[1:])])
    j += Constant(0.5 * problem.beta/dt) * time_regularisation * dsg_

else:
    h1_regularisation = sum([u_i**2 + t_grad(u_i)**2 for u_i in u_values])
    dt = problem.t_range[1] - problem.t_range[0]
    j += Constant(0.5 * problem.alpha*dt) * h1_regularisation * ds

    # H1 regularisation in time
    time_regularisation = sum([(u_i-u_j)**2 +  t_grad(u_i-u_j)**2 for u_i, u_j in zip(u_values[:-1], u_values[1:])])
    j += Constant(0.5 * problem.beta/dt) * time_regularisation * ds


# Enforce periodicity via quadratic penalty term
periodic_regularisation = (g.values()[0]-u)**2*dx
j += Constant(0.5 * problem.periodicity_alpha) * periodic_regularisation

# Scale functional
scale = 1
j = Constant(scale)*j
J = Functional(j)


###################### Optimisation setup ############################
parameters["adjoint"]["stop_annotating"] = True

# Define the dolfin-adjoint controls
c = [Control(problem.u0)]  # initial condition
c += [Control(gg) for gg in g.itervalues()]  # bc conditions

# Compute and plot the gradient (optional)
#dJ = compute_gradient(J, c, project=False, forget=False)
#for i, t in enumerate(problem.t_range):
#   plot(dJ[i], title="dJ, t = %f" % t)
#    interactive()

# Set up the optimization
def jhat_eval_pre(*args):
    print0("Running forward model...")
def jhat_eval_post(j, *args):
    print0(" j = {:.2e}".format(j))
def jhat_deriv(*args):
    print0("Running adjoint model")
def replay_cb(var, value, m):
    if "Control" in var.var.name:
        maxval = MPI.max(mpi_comm_world(), max(abs((value.vector().array()))))
        print0(" max({}) = {}".format(var.var.name, maxval))

Jhat = ReducedFunctional(J, c, eval_cb_pre=jhat_eval_pre,
                               eval_cb_post=jhat_eval_post,
                               derivative_cb_pre=jhat_deriv,
                               replay_cb=replay_cb)
#dJ = Jhat.derivative()
#plot(dJ[5], interactive=True)

# Perform a sanity check. Test that the manually computed functional value
# is equal to the evaluation of the reduced functional for the same inputs
ctrl_vals = [u00] + g.values()
if problem.dim == 2:
    assert abs(Jhat(ctrl_vals) - assemble(j)) < 1e-5/scale

    #set_log_level(INFO)
    #print "Min taylor convergence: ", Jhat.taylor_test(ctrl_vals,
    #        test_hessian=True)
    #exit()

# Generate a good initial guess from the observations
else:
    # Initial condition
    u00.vector()[:] = obs[0].vector()
    # Boundary controls
    #i = 0
    #for t in problem.t_range[1:]:
    #    if t >= problem.obs_t_range[min(i+1, len(problem.obs_t_range)-2)]:
    #        i = min(i+1, len(problem.obs_t_range)-2)
    #    g[t].vector()[:] = obs[problem.obs_t_range[i]].vector()
    # Rerun forward model with new controls
    Jhat(ctrl_vals)

###################### Optimize ############################

if problem.opt_method == "scipy":
    opt = minimize(Jhat, options={"maxiter": problem.opt_maxiter-2}, tol=1e-10)

elif problem.opt_method == "moola":
    opt_problem = MoolaOptimizationProblem(Jhat)
    assert len(problem.moola_norm) == 4
    time_norm = problem.moola_norm[:2]
    spce_norm = problem.moola_norm[2:]

    def inv(norm_type): return "lu" if problem.moola_norm_exact else ("sor" if norm_type == "L2" else "amg")
    if spce_norm in ("L2", "H1"):
        Q = problem.control_space
        h0 = inner(TrialFunction(Q), TestFunction(Q)) * dsg_
        h1 = inner(t_grad(TrialFunction(Q)), t_grad(TestFunction(Q))) * dsg_  

        form = h0 if spce_norm == "L2" else h0 + h1
        form += Constant(0.0)*inner(TrialFunction(Q), TestFunction(Q)) * dx

        M = assemble(form) #, keep_diagonal=True)

        bcs_walls = [DirichletBC(Q, (0.,)*problem.dim, problem.facet_ids, id_) for id_ in problem.wall_ids]
        for bc in bcs_walls:
            as_backend_type(M).mat().zeroRowsColumns(bc.get_boundary_values().keys())
        try:
            M.ident_zeros(1e-10)
        except:
            d = Function(Q).vector()
            M.get_diagonal(d)
            d_local = d.get_local()
            d_local[numpy.where(abs(d_local)<1e-10)] = 1.
            d.set_local(d_local)
            M.set_diagonal(d)

        rm_bndary = moola.adaptors.dolfin_vector.RieszMap(Q, inner_product = "custom",
                                                          map_operator = M, inverse = inv(spce_norm))

        rm_domain = moola.adaptors.dolfin_vector.RieszMap(problem.u0.function_space(), inner_product = spce_norm, inverse = inv(spce_norm))

    elif spce_norm == "l2":
        rm_domain = moola.adaptors.dolfin_vector.IdentityMap()
        rm_bndary = moola.adaptors.dolfin_vector.IdentityMap()

    else:
        raise ValueError("Unhandled spatial norm: {}".format(spce_norm))

    if time_norm in("L2", "H1"):
        import numpy
        def h0(N, dt):
            m = numpy.eye(N,N, 0) * 4 \
              + numpy.eye(N,N, 1) \
              + numpy.eye(N,N,-1)

            return m * (dt/ 6)

        def h1(N, dt):
            a = numpy.eye(N, N) * 2 \
              - numpy.eye(N, N, -1) \
              - numpy.eye(N, N,  1)

            return  a/dt

        N, dt = len(ctrl_vals)-1, problem.t_range[1] - problem.t_range[0]

        H = h0(N, dt) * problem.alpha

        if time_norm == "H1":
            H += (h1(N, dt) + h0(N, dt)) * problem.beta

        # scale the end points
        H[ 0, 0] /= 2
        H[-1,-1] /= 2


        M = numpy.zeros((N+1,N+1)) # initial condition is also controlled
        M[ 0, 0] = problem.gamma if problem.gamma != 0 else 1.0
        M[1:,1:] = H
        rm_time = moola.adaptors.dolfin_vector_set.RieszMapSet("custom", map_operator = M)

    elif time_norm == "l2":
        rm_time = moola.adaptors.dolfin_vector_set.IdentityMapSet()

    else:
        raise ValueError("Unhandled temporal norm: {}".format(time_norm))

    moola_vecs = [moola.DolfinPrimalVector(ctrl_vals[0], riesz_map = rm_domain)] \
               + [moola.DolfinPrimalVector(ctrl_vals[i], riesz_map = rm_bndary) for i in xrange(1,len(ctrl_vals))]

    moola_multi_vec = moola.DolfinPrimalVectorSet(moola_vecs, riesz_map = rm_time)
    solver = moola.BFGS(opt_problem, moola_multi_vec,
                        options={'rgtol':  problem.opt_rgtol,
                                 'rjtol':  problem.opt_rjtol,
                                 'maxiter': problem.opt_maxiter,
                                 'display': 3,
                                 #'mem_lim': 0,
                             })
    sol = solver.solve()
    opt = sol['control'].data
    print moola.events

elif problem.opt_method == "ipopt":
    optproblem = MinimizationProblem(Jhat)
    parameters = {"acceptable_tol": 1.0e-200,
                  "maximum_iterations": problem.opt_maxiter,
                  "print_level": 5}
    solver = IPOPTSolver(optproblem, parameters=parameters)
    opt = solver.solve()

elif problem.opt_method == "tao":
    optproblem = MinimizationProblem(Jhat)
    parameters = { "monitor": None,
                   "type": "lmvm",
                   "max_it": problem.opt_maxiter,
                   "subset_type": "matrixfree",
                   "fatol": 0.0,
                   "frtol": 1e-9,
                   "gatol": 0.0,
                   "grtol": 0.0,
                 }
    solver = TAOSolver(optproblem, parameters=parameters)
    opt = solver.solve()

# Split optimal solution in its components
u0_opt = opt[0]
g_opt = OrderedDict(zip(problem.t_range[1:], opt[1:]))

# Write optimised results
solver_params["annotate"] = False
results_writer = ResultsWriter(problem.t_range, problem.output_dir)

u_values = []
for t, u, p in ns_solver.run(problem.t_range, u0_opt, g_opt, **solver_params):
    # Project the velocity state into the observation space
    #u_values.append(Function(u))
    u_values.append(project(u, u.function_space(), annotate = annotate,
            solver_type="lu"))
    u_t = project(u, problem.observation_space, name="u_optimal_{}".format(t),
            solver_type="lu")
    integrator.update(t, u_t)
    results_writer.write_state(t, u, p, g_opt)

#u_values.pop(0) # removing initial condition

results_writer.write_observations(integrator, problem.obs_t_range, problem.time_average_observations)
print0("Periodicity regularisation {:e}".format(assemble(Constant(0.5 * problem.periodicity_alpha) * (g_opt.values()[0]-u)**2*dx)))
print0("H1-time regularisation {:e}".format(assemble(Constant(0.5 * problem.beta/dt) * sum([((g1-g0)**2 + t_grad(g1-g0)**2)*dsg_ for g1, g0 in zip(u_values[:-1], u_values[1:])]))))
print0("L2-space regularisation {:e}".format(assemble(Constant(0.5 * problem.alpha*dt) * sum([(g0**2 + t_grad(g0)**2)*dsg_ for g0 in u_values]))))
print0("Initial condition regularisation {:e}".format(assemble(Constant(0.5 * problem.gamma) * (inner(u0_opt, u0_opt) + grad(u0_opt)**2)*dx)))

