from dolfin import *
from problem import *
from utils import FileReader, FileWriter
from matplotlib import pylab as plt
parameters["plotting_backend"] = "matplotlib"

############### Read the command line arguments #################
parser = argparse.ArgumentParser()
parser.add_argument('--infile', required=True, help='path to observation file')
parser.add_argument('--discretization', required=True, help='Finite Element discretization')
args = parser.parse_args()
args.mode = "generate"

fr_obs = FileReader(args.infile)
problem = Aneurysm2D(args)


t = "0.296"
u = Function(problem.observation_space)
fr_obs.read(u, "obs", t)


Vs = FunctionSpace(problem.observation_space.mesh(), "CG", 1)
umag = project(inner(u,u)**0.5, Vs)

x = plot(umag, mode="color")

cmap = plt.cm.get_cmap('coolwarm', 11)    # 11 discrete colors

x.set_cmap(cmap)
x.set_clim(0, 850)

#from IPython import embed; embed()

plt.axis('off')
plt.savefig("test.pdf")
