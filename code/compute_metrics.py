import seaborn as sns
import subprocess
import argparse
from collections import namedtuple
from dolfin import Function
from problem import *
from utils import FileReader, FileWriter
import matplotlib
import matplotlib.pyplot as plt
import numpy
import re

sns.set(font_scale=2)  # crazy big
matplotlib.rc('text', usetex=True)

set_log_level(ERROR)

############### Read the command line arguments #################
parser = argparse.ArgumentParser()
parser.add_argument('--problem', required=True, choices=["Bifurcation2D", "Aneurysm2D", "Dog3D"], help='name of the probem')
parser.add_argument('--u_true', required=True, help='path to true velocity state file')
parser.add_argument('--u_assim', required=True, help='path to assimilated velocity state file')
parser.add_argument('--logfile', required=True, help='destination of log file, needed to extract convergence and iteration information')
parser.add_argument('--outfile', required=True, help='difference output file')
parser.add_argument('--statdir', required=True, help='output directory where to store fiels with more information')
parser.add_argument('--nu', required=True, help='viscosity')
parser.add_argument('--discretization', required=True, help='Finite Element discretization')

args = parser.parse_args()

us_true = FileReader(args.u_true)
us_assim = FileReader(args.u_assim)
diff_out = FileWriter(args.outfile)

# Add dummy parameters
sys.argv = ["", "--output_dir", "_", "--problem", args.problem, "--nu", args.nu,
            "--discretization", args.discretization]
problem = create_problem_from_commandline("generate")

problemvelocity_space = VectorFunctionSpace(problem.mesh, "CG", 1)
u_true = Function(problemvelocity_space)

submesh = problem.observation_space.mesh()
degree = 1 # problem.velocity_space.element().ufc_element().degree()
V = VectorFunctionSpace(submesh, "CG", degree)
u_assim = Function(V)

# Get timestep
dts = problem.t_range[1:] - problem.t_range[:-1]
dt = dts[0]
assert max(abs(dts - dt)) < 1e-14

quad_weights = numpy.ones(len(problem.t_range))*dt
quad_weights[0] *= 0.5
quad_weights[-1] *= 0.5

class AneurysmBoundary(SubDomain):
    def inside(self, x, on_boundary):
        # Carefully handcrafted values to select the aneurysm boundary
        return (-(x[0]-2.1) + 0.8*(x[1]-8.475) > 0 and
                (x[0]-0.3) + 0.5*(x[1]-7.85)  > 0 and
                -1.5 < x[0] < 4.5 and
                on_boundary)

class AneurysmArea(SubDomain):
    def inside(self, x, on_boundary):
        # Carefully handcrafted values to select the aneurysm boundary
        return (-(x[0]-2.1) + 0.8*(x[1]-8.475) > 0 and
                (x[0]-0.3) + 0.5*(x[1]-7.85)  > 0 and
                -1.5 < x[0] < 4.5 and
                -0.4*(x[0]-0.3) + 1.0*(x[1]-8.1)  > 0
                )

sub_domains = FacetFunction("size_t", submesh, submesh.topology().dim() - 1)
sub_domains.set_all(0)
AneurysmBoundary().mark(sub_domains, 1)
#plot(sub_domains, interactive=True)
ds_aneurysm = Measure("ds")(subdomain_data=sub_domains)(1, domain=submesh)

sub_domains = CellFunction("size_t", submesh, submesh.topology().dim())
sub_domains.set_all(0)
AneurysmArea().mark(sub_domains, 1)
#plot(sub_domains, interactive=True)
dx_aneurysm = Measure("dx")(subdomain_data=sub_domains)(1, domain=submesh)

def epsilon(u):
    return sym(grad(u))


def compute_stresses(u, nu):
    # Note, the p term does not matter, because it vanishes in the WSS definition
    p = Constant(0)
    rho = 1060. # kg/m**3

    n = FacetNormal(submesh)
    # compute stress tensor
    sigma = 2*nu*epsilon(u) - p*Identity(len(u))
    # Compute surface traction

    T = -sigma*n

    # Compute normal and tangential components
    Tn = inner(T, n) # scalar-valued
    Tt = T - Tn*n # vector-valued

    # Compute integral of wall shear stress over aneurysm facets
    Lt = Tt**2*ds_aneurysm
    return rho*assemble(Lt)**0.5


# Will be u_true - u_diff
u_diff = Function(V)
u_diff_vec = u_diff.vector()

shear_stresses_true = []
shear_stresses_assim = []
shear_stresses_diff = []

uns_true = []
uns_assim = []
uns_diff = []

uns_aneurysm_true = []
uns_aneurysm_assim = []
uns_aneurysm_diff = []

for t, w in zip(problem.t_range, quad_weights):

    us_true.read(u_true, "u", t)
    us_assim.read(u_assim, "u", t)

    # Map the true velocity to the observation domain
    try:
        u_true_proj = interpolate(u_true, V)
    except RuntimeError:
        u_true_proj = project(u_true, V)

    u_diff_vec[:] = u_true_proj.vector()
    u_diff_vec[:] -= u_assim.vector()

    uns_diff.append(norm(u_diff, "L2"))
    uns_true.append(norm(u_true_proj, "L2"))
    uns_assim.append(norm(u_assim, "L2"))

    uns_aneurysm_diff.append(assemble(u_diff**2*dx_aneurysm)**0.5)
    uns_aneurysm_true.append(assemble(u_true_proj**2*dx_aneurysm)**0.5)
    uns_aneurysm_assim.append(assemble(u_assim**2*dx_aneurysm)**0.5)

    shear_stresses_diff.append(compute_stresses(u_diff, nu=problem.args.nu))
    shear_stresses_true.append(compute_stresses(u_true_proj, nu=problem.args.nu))
    shear_stresses_assim.append(compute_stresses(u_assim, nu=problem.args.nu))


# Time-space norms
shear_stress_int_diff = numpy.trapz(numpy.array(shear_stresses_diff)**2, dx=dt)**0.5
shear_stress_int_true = numpy.trapz(numpy.array(shear_stresses_true)**2, dx=dt)**0.5
shear_stress_int_assim = numpy.trapz(numpy.array(shear_stresses_assim)**2, dx=dt)**0.5

u_int_diff = numpy.trapz(numpy.array(uns_diff)**2, dx=dt)**0.5
u_int_true = numpy.trapz(numpy.array(uns_true)**2, dx=dt)**0.5
u_int_assim = numpy.trapz(numpy.array(uns_assim)**2, dx=dt)**0.5

u_aneurysm_int_diff = numpy.trapz(numpy.array(uns_aneurysm_diff)**2, dx=dt)**0.5
u_aneurysm_int_true = numpy.trapz(numpy.array(uns_aneurysm_true)**2, dx=dt)**0.5
u_aneurysm_int_assim = numpy.trapz(numpy.array(uns_aneurysm_assim)**2, dx=dt)**0.5

# Plot the shear stresses, scaled by its time average

# plotting labels
label_u = "$\\Vert u(t)\\Vert$"
label_t = "$\\Vert u_{{true}}(t)\\Vert$"
label_d = "$\\Vert u_{{true}}(t)-u(t)\\Vert$"

label_wss_u = "$\\Vert (t)\\Vert$"
label_wss_t = "$\\Vert \\operatorname{WSS}_{{true}}(t)\\Vert$"
label_wss_d = "$\\Vert \\operatorname{WSS}_{{true}}(t) - \\operatorname{WSS}(t) - \\Vert$"


linewidth = 4

C = numpy.trapz(shear_stresses_true, dx=dt)/problem.t_range[-1]
p = plt.plot(problem.t_range, shear_stresses_true/C, label=label_wss_t)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, shear_stresses_assim/C, label=label_wss_u)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, shear_stresses_diff/C, label=label_wss_d)
plt.setp(p, linewidth=linewidth)
plt.xlabel("time (s)")
#plt.ylabel("$\|\cdot\|_{\\partial \\Omega_{Aneurysm}}$")
plt.ylim(0, 2.4)
#plt.legend()
plt.savefig(args.statdir + "/scaled_wss.pdf", format="pdf", bbox_inches='tight')

plt.figure()

# Plot the velocities, scaled by its time average
C = numpy.trapz(uns_true, dx=dt)/problem.t_range[-1]
p = plt.plot(problem.t_range, uns_true/C, label=label_t)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, uns_assim/C, label=label_u)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, uns_diff/C, label=label_d)
plt.setp(p, linewidth=linewidth)
plt.xlabel("time (s)")
#plt.ylabel("$\|\cdot\|_{\\Omega}$")
plt.ylim(0, 1.8)
#plt.legend()
plt.savefig(args.statdir + "/scaled_velocities.pdf", format="pdf",
        bbox_inches='tight')

vne_fig = plt.figure()

# Plot the velocities in the aneurysm, scaled by its time average
C = numpy.trapz(uns_aneurysm_true, dx=dt)/problem.t_range[-1]
p = plt.plot(problem.t_range, uns_aneurysm_true/C, label=label_t)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, uns_aneurysm_assim/C, label=label_u)
plt.setp(p, linewidth=linewidth)
p = plt.plot(problem.t_range, uns_aneurysm_diff/C, label=label_d)
plt.setp(p, linewidth=linewidth)
plt.xlabel("time (s)")
#plt.ylabel("$\|\cdot\|_{\\Omega_{Aneurysm}}$")
plt.ylim(0, 2.4)
#plt.legend()

plt.savefig(args.statdir + "/scaled_velocities_aneurysm.pdf",
        format="pdf", bbox_inches='tight')

# legends as a separate figure
labels = ["True", "Assimilated", "Error"]
lfig = plt.figure()
lfig.legend(vne_fig.axes[0].lines, labels, ncol = 3)
lfig.set_figwidth(6.75)
lfig.set_figheight(0.775)
lfig.savefig(args.statdir + "/labels.pdf",
             format="pdf")


# Get optimisation convergence and iteration numbers
logfile = open(args.logfile, "r")
converged = False
num_iter = -1
for l in logfile:
    if "Tolerance reached: delta_j < jtol" in l:
        converged = True
    scan = re.match("iteration = ([0-9]*):\tobjective = ([0-9]*\.[0-9]*)", l)
    if scan:
        num_iter, j = int(scan.groups()[0]), float(scan.groups()[1])
logfile.close()

statfile = open(args.statdir + "/metrics.tex", "w")
statfile.write("Opt.-iters.: ${}$\\\\\n".format(num_iter))
statfile.write("$J+\mathcal R = {0:.0f}\\times 10^{{5}}$\\\\\n".format(j/100000))
#statfile.write("$\mathcal E_{{\\Omega_{{\\text{{obs}}}}}} = {0:.3f}$\\\\\n".format(u_int_diff/u_int_true))
#statfile.write("$\mathcal E_{{u}}^{{abs}} = {0:.0f}$\\\\\n".format(u_int_diff))
statfile.write("$\mathcal E_{{\\Omega_{{\\text{{ane}}}}}} = {0:.0f} \\%$\\\\\n".format(u_aneurysm_int_diff/u_aneurysm_int_true*100))
#statfile.write("$\mathcal E_{{ua}}^{{abs}} = {0:.0f}$\\\\\n".format(u_aneurysm_int_diff))
statfile.write("$\mathcal E_{{\\text{{WSS}}}} = {0:.0f} \\%$\\\\\n".format(shear_stress_int_diff/shear_stress_int_true*100))
#statfile.write("$\mathcal E_{{stress}}^{{abs}} = {0:.0f}$\\\\\n".format(shear_stress_int_diff))
#statfile.write("Converged: {}\\\\\n".format(converged))

statfile.close()
