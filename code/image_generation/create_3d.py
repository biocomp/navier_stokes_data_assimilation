from paraview.simple import *
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--xdmf_file", type = str, required = True,
                    help = "Input data file"),
parser.add_argument("--out_file", type = str, required = True,
                    help = "Output file name")
parser.add_argument("--time", type = float, required = False, default = 0.296,
                    help = "Time for snapshot")
parser.add_argument("--offset", required = False, 
                    action = "store_true",
                    help = "apply time offset")



args = parser.parse_args()
time = args.time
xdmf_file = args.xdmf_file
out_file = args.out_file
T = 0.592
t0 = 0

if args.offset:
    time += 0.592
    T    += 0.592
    t0   += 0.592
    x0, x1, x2 = [90.1939332781083, 70.0967948466333, 123.786196133447]
    field = "Velocity"
else:
    x0, x1, x2 = [87.4731987372204, 72.4306759643166, 107.300212130508]
    field = "obs"
"""
T = 0.592
t0 = 0  
time = 0.296
x0, x1, x2 = [87.4731987372204, 72.4306759643166, 107.300212130508]

#xdmf_file='/home/magneano/Work/nsassimilation/source/results_3d/Velocity.xdmf'

xdmf_file="/home/magneano/Work/nsassimilation/source/results_dog/instant/assimilated_H1H1_3_level/obs_assimilated.xdmf"
out_file = "/home/magneano/Work/nsassimilation/source/image_generation/test.png"
"""

velocity_data = XDMFReader(FileName=xdmf_file)
velocity_data.PointArrays = [field]


# following appears not be needed
#velocity_data.Grids = ['Velocity_0', 'Velocity_1', 'Velocity_2', 'Velocity_3', 'Velocity_4', 'Velocity_5', 'Velocity_6', 'Velocity_7', 'Velocity_8', 'Velocity_9', 'Velocity_10', 'Velocity_11', 'Velocity_12', 'Velocity_13', 'Velocity_14', 'Velocity_15', 'Velocity_16', 'Velocity_17', 'Velocity_18', 'Velocity_19', 'Velocity_20', 'Velocity_21', 'Velocity_22', 'Velocity_23', 'Velocity_24', 'Velocity_25', 'Velocity_26', 'Velocity_27', 'Velocity_28', 'Velocity_29', 'Velocity_30', 'Velocity_31', 'Velocity_32', 'Velocity_33', 'Velocity_34', 'Velocity_35', 'Velocity_36', 'Velocity_37', 'Velocity_38', 'Velocity_39', 'Velocity_40', 'Velocity_41', 'Velocity_42', 'Velocity_43', 'Velocity_44', 'Velocity_45', 'Velocity_46', 'Velocity_47', 'Velocity_48', 'Velocity_49', 'Velocity_50', 'Velocity_51', 'Velocity_52', 'Velocity_53', 'Velocity_54', 'Velocity_55', 'Velocity_56', 'Velocity_57', 'Velocity_58', 'Velocity_59', 'Velocity_60', 'Velocity_61', 'Velocity_62', 'Velocity_63', 'Velocity_64', 'Velocity_65', 'Velocity_66', 'Velocity_67', 'Velocity_68', 'Velocity_69', 'Velocity_70', 'Velocity_71', 'Velocity_72', 'Velocity_73', 'Velocity_74', 'Velocity_75', 'Velocity_76', 'Velocity_77', 'Velocity_78', 'Velocity_79', 'Velocity_80', 'Velocity_81', 'Velocity_82', 'Velocity_83', 'Velocity_84', 'Velocity_85', 'Velocity_86', 'Velocity_87', 'Velocity_88', 'Velocity_89', 'Velocity_90', 'Velocity_91', 'Velocity_92', 'Velocity_93', 'Velocity_94', 'Velocity_95', 'Velocity_96', 'Velocity_97', 'Velocity_98', 'Velocity_99', 'Velocity_100']

# stuff from trace
animation_scene = AnimationScene(PlayMode='Snap To TimeSteps',
                                 StartTime = t0,
                                 EndTime = T,
                                 AnimationTime = time)

velocity_representation = Show(ScaleFactor                      = 8.562440490722656,
                               ScalarOpacityUnitDistance        = 0.5414068757845057,
                               SelectionPointFieldDataArrayName = field, 
                               Opacity                          = 0.3, 
                               EdgeColor                        = [0.0, 0.0, 0.5000076295109483])


velocity_representation.Visibility = 1



tracer = StreamTracer( SeedType="Point Source")
tracer.SeedType.Radius = 2.0
tracer.SeedType.NumberOfPoints = 150
#tracer.SeedType.Center = [90.0887237441032, 69.85677412780731, 123.76978465720822]
tracer.SeedType.Center = [x0, x1, x2]
tracer.Vectors = ['POINTS', field]
tracer.SeedType = "Point Source"
tracer.MaximumStreamlineLength = 150.0



tracer_representation = Show(SelectionCellFieldDataArrayName  = 'ReasonForTermination', 
                             ScaleFactor                      = 8.545139694213868,
                             SelectionPointFieldDataArrayName = 'AngularVelocity', 
                             EdgeColor                        = [0.0, 0.0, 0.5000076295109483] )

tracer_representation.Visibility = 0


# make tubes
SetActiveSource(tracer)
tube = Tube(Scalars = ['POINTS', 'AngularVelocity'], 
            Vectors = ['POINTS', 'Normals'],
            Radius  = 0.1)


SetActiveSource(tube)
tube_representation = Show(SelectionCellFieldDataArrayName  = 'ReasonForTermination',
                           SelectionPointFieldDataArrayName = 'AngularVelocity',
                           ScaleFactor                      = 8.57020835876465, 
                           EdgeColor                        = [0.0, 0.0, 0.5000076295109483])

obs_PVLookupTable = GetLookupTableForArray(field, 3 , RGBPoints=[0.0, 0.23, 0.299, 0.754, 500.0, 0.706, 0.016, 0.15], LockScalarRange=1 )
tube_representation.ColorArrayName = ('POINT_DATA', field)
tube_representation.LookupTable = obs_PVLookupTable

tube_representation.Visibility = 1
render_view = GetRenderView()
render_view.ViewTime = time 
render_view.CacheKey = time 
render_view.UseCache = 0

render_view.CameraFocalPoint    = [83.61699353457793, 72.23177081065361, 77.13004931402355]

#render_view.CameraPosition      = [122.39645401749955, 72.640516096385, 72.45747069760596]
#render_view.CameraClippingRange = [13.328048003994187, 71.34517512521244]
render_view.CameraPosition      = [115.66613442955446, 72.56957683191922, 73.26841409384372]
render_view.CameraClippingRange = [6.616471285605677, 64.4641141462586]

render_view.CameraParallelScale = 46.21764939273498

render_view.CenterAxesVisibility = 0
render_view.OrientationAxesVisibility = 0

render_view.CenterOfRotation    = [84.29089736938477, 65.11694526672363, 81.3427963256836]


print t0, time, T

WriteImage(out_file, Magnification=1)
Render()
