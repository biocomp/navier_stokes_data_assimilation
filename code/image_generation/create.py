from paraview.simple import *
import subprocess
import sys

pvsm_base = "aneurysm"
pvsm = pvsm_base+".pvsm"
pvsm_tmp = "tmp.pvsm"

oldpath = "/home/simon/src/nsassimilation/source/results_aneurysm/instant/assimilated_H1H1_0_noise/obs.xdmf"
zoom = sys.argv[1]=="--zoom"
if zoom:
    del sys.argv[1]
newpath = sys.argv[1]
outfile = sys.argv[2]
fieldname = sys.argv[3]

subprocess.Popen("cp {} {}".format(pvsm, pvsm_tmp), shell=True).wait()
subprocess.Popen("sed -i 's;{};{};' {}".format(oldpath, newpath, pvsm_tmp), shell=True).wait()
subprocess.Popen("sed -i 's;{};{};' {}".format("obs", fieldname, pvsm_tmp), shell=True).wait()


# Load the state
servermanager.LoadState(pvsm_tmp)
# Make sure that the view in the state is the active one
SetActiveView(GetRenderView())
# Now render
renderView1 = GetRenderView()
renderView1.ViewSize = [800, 800]  # Use this to increase resolution
#renderView1.OrientationAxesVisibility = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.ResetCamera()

Render()

if zoom:
    renderView1.CameraFocalPoint = [1.3, 10.0, 90.0999984741211]  # Change this to move camera
    renderView1.CameraParallelScale = 2.89
    renderView1.CameraPosition = [0.0, 0.0, 1000.]

max_val = 1000.

obsLUT = GetColorTransferFunction(fieldname)
obsLUT.RescaleTransferFunction(0.0, max_val)
obsPWF = GetOpacityTransferFunction(fieldname)
obsPWF.RescaleTransferFunction(0.0, max_val)

# Work around for Paraview
# get animation scene
animationScene1 = GetAnimationScene()
animationScene1.GoToPrevious()
animationScene1.GoToNext()


WriteImage(outfile, Magnification=1)

# Crop image
subprocess.Popen("convert {0} -trim {0}".format(outfile).split()).wait()

subprocess.Popen("rm {}".format(pvsm_tmp).split()).wait()
