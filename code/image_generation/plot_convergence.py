import sys
from matplotlib import pyplot



def parseline(line):
    line = line.strip(" :\t\n")
    d = {}
    for (i,w) in enumerate(line.split(":")):
        w = w.strip(" \t\n")
        w1, w2 = w.split(" = ")
        d[w1] = eval(w2)
    return d["iteration"], d["objective"], d["grad_norm"]
            
    
def extract_data(log):    
    data = {}
    for line in log:
        if line.startswith("iteration"):
            it, obj, grad = parseline(line)
            if it not in data.keys():
                data[it] = {"objective": obj, "grad_norm": grad}
                data[it].update({"rel_grad": grad/data[0]["grad_norm"]})
                data[it].update({"rel_obj": obj/data[0]["objective"]})
                data[it].update({"delta": None if it == 0 else data[it-1]["objective"] - obj})
                data[it].update({"rdelta": None if it == 0 else (data[it-1]["objective"] - obj)/data[1]["delta"]})
    return data


def plot_data(data, field, logplot = False, **kwargs):
    from numpy import array, zeros
    iters = array(data.keys())
    iters.sort()
    values = zeros(len(iters))
    values[:] = [data[i][field] for i in iters]
    if logplot:
        p = pyplot.loglog(iters, values, **kwargs)
    else:
        p = pyplot.plot(iters, values, **kwargs)
    return p
    
def make_plot_from_log(filename, field, logplot = False, **kwargs):
    infile = open(filename)
    log = infile.readlines()
    infile.close()
            
    data = extract_data(log)        
    p = plot_data(data, field, logplot = logplot,  **kwargs)

if __name__ == "__main__":
    filenames = sys.argv[1:]
    for filename in filenames:
        p = make_plot_from_log(filename, "rdelta")

    pyplot.show()
