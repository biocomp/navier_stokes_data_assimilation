find ../results_aneurysm -iname obs*.xdmf -exec bash -c 'python create.py --zoom ${0} ${0%.*}_zoom.png obs' {} \;
find ../results_aneurysm -iname obs*.xdmf -exec bash -c 'python create.py ${0} ${0%.*}.png obs' {} \;
find ../results_aneurysm -iname diff.xdmf -exec bash -c 'python create.py ${0} ${0%.*}.png diff' {} \;
find ../results_aneurysm -iname diff*.xdmf -exec bash -c 'python create.py --zoom ${0} ${0%.*}_zoom.png diff' {} \;
