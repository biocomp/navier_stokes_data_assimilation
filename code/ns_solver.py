from fenics import *
from dolfin_adjoint import *
from utils import print0


class NSSolver(object):

    def __init__(self, problem):
        self.problem = problem
        self.dx = Measure('dx', domain=problem.mesh)
        self.ds = Measure('ds', domain=problem.mesh, subdomain_data=problem.facet_ids)

    def __getattr__(self, atrrname):
        return getattr(self.problem, atrrname)

    def run(self, t_range, u0, g, velocity_space, nu,
                  nitsche_method = "symmetric",
                  nitsche_gamma = 10.,
                  convection = False,
                  nonlinear_method = "picard",
                  theta=0.5,
                  annotate=False):

        mesh = self.mesh
        print "Mesh cells: ", mesh.num_cells()
        print "Mesh facets: ", mesh.num_facets()
        print "Mesh edges: ", mesh.num_edges()
        print "Mesh vertices: ", mesh.num_vertices()
        print "="*20
        dt = Constant(t_range[1] - t_range[0])
        theta = Constant(theta)
        nu = Constant(nu)


        V = velocity_space                      # Velocity
        #V_element = V.ufl_element()

        #Q_element = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        #W_element = MixedElement([V_element, Q_element])

        #Q = FunctionSpace(mesh, Q_element)        # Pressure
        #W = FunctionSpace(mesh, W_element)
        Q = FunctionSpace(mesh, "CG", 1)
        W = MixedFunctionSpace([V, Q])
        control_space = g.itervalues().next().function_space()

        v, q = TestFunctions(W)
        U = Function(W, name="Solution")
        p0 = Function(Q, name="Pressure")
        n = FacetNormal(mesh)

        if nonlinear_method == "picard":
            u, p = TrialFunctions(W)
            beta = Function(V, name="Wind function")
        else:
            u, p = split(U)
            beta = U

        # Define measures over mesh and facet_ids
        dx = self.dx
        ds = self.ds
        # Boundary measure where only weak velocity bcs are defined
        dsu_ = self.dsu_ = ds(self.controlled_velocity_facet_ids)
        # Boundary measure where only weak pressure bcs are defined
        # Note: Really we control grad u/grad n + p.n
        dsp_ = self.dsp_ = ds(self.controlled_pressure_facet_ids)

        # Create boundary conditions
        zero = (0,)*self.dim

        bcu_strong = [DirichletBC(W.sub(0), zero, self.facet_ids, id_)
                      for id_ in self.wall_ids]
        bcp_strong = [] # DirichletBC(W.sub(1), 0, facet_ids, inflow_id)

        # Define variational form
        u_theta = theta*u + (1-theta)*u0
        g_theta = Function(control_space, name="g_theta")

        # Standard Stokes terms
        F = (
            1.0/Constant(dt)*inner(u-u0,v)*dx()
             + nu*inner(grad(u_theta), grad(v))*dx()
             - nu*inner(grad(u_theta)*n, v)*dsu_
             - inner(p, div(v))*dx()
             + inner(p*n, v)*dsu_
             - inner(q, div(u))*dx()
             #+ inner(grad(q), u)*dx()
             #- q*inner(u, n)*dsu_
            )

        # Add stabilisation term for P1-P1 element
        if V.ufl_element().degree() == 1:
            beta = Constant(0.001)
            h = CellSize(mesh)
            F -= beta*h**2*inner(grad(p), grad(q))*dx()
            print0("Adding P1-P1 stabilisation term (beta={})".format(float(beta)))

        # Add convection for Navier Stokes
        if convection and nonlinear_method == "picard":
            F += theta*inner(grad(u)*beta, v)*dx() + (1-theta)*inner(grad(u0)*u0, v)*dx()
        if convection and nonlinear_method == "newton":
            F += inner(grad(u_theta)*u_theta, v)*dx()

        # Weakly enforced boundary terms
        F -= inner(g_theta, v)*dsp_
        if nitsche_method == "symmetric":
            F += (
                - theta*nu*inner(grad(v)*n, u)*dsu_
                + theta*nu*inner(grad(v)*n, g_theta)*dsu_
                + inner(q*n, u)*dsu_
                - inner(q*n, g_theta)*dsu_
                )
        else:
            F += (
                - theta*nu*inner(grad(v)*n, u)*dsu_
                + theta*nu*inner(grad(v)*n, g_theta)*dsu_
                #+ inner(q*n, u)*dsu_
                #- inner(q*n, g_theta)*dsu_
                # Appears to stable without the last two terms here
                )

        # Add the Nitsche stabilization terms
        if nitsche_gamma > 0:
            h = CellSize(mesh)
            F +=(+ theta*nitsche_gamma*nu/h*inner(u,v)*dsu_
                 - theta*nitsche_gamma*nu/h*inner(g_theta,v)*dsu_)

        u_assigner = FunctionAssigner(V, W.sub(0))
        p_assigner = FunctionAssigner(Q, W.sub(1))

        # Extract lhs and rhs
        if nonlinear_method == "picard":
            a = lhs(F)
            L = rhs(F)

            b = assemble(L)
            A = assemble(a)

            # Fixed point tolerance
            FP_TOL = 1e-8

        # Initial condition
        yield t_range[0], u0, p0

        for t in t_range[1:]:
            # Update boundary condition
            g_theta.assign(g[t], annotate=annotate)

            if nonlinear_method == "picard":
                # Re-assemble rhs
                assemble(L, tensor=b)

                # Fixed point iteration
                err = 1e16
                i = 0
                while err > FP_TOL:
                    # Re-assemble
                    assemble(a, tensor=A)

                    # Apply strong boundary conditions
                    for bc in bcu_strong:
                        bc.apply(A)
                        bc.apply(b)

                        # Solve
                        solve(A, U.vector(), b)

                        u_tmp, p_tmp = U.split()
                        u_tmp = Function(u_tmp, name="u_tmp")

                        #err = errornorm(u_tmp, beta, degree_rise=0)/(norm(beta)+1e-8)
                        err = norm(u_tmp.vector()-beta.vector())/(norm(beta.vector())+1e-8)

                        if MPI.rank(mpi_comm_world()) == 0:
                            print("Picard iteration {} Error: {}".format(i, err))

                    u_assigner.assign(beta, U.sub(0))

                    i += 1
            else:
                problem = NonlinearVariationalProblem(F, U, bcu_strong +
                        bcp_strong, derivative(F, U))
                solver = NonlinearVariationalSolver(problem)
                solver.parameters["newton_solver"]["linear_solver"] = "mumps"
                #solver.parameters["newton_solver"]["linear_solver"] = "superlu_dist"
                timer = Timer("Forward solve")
                solver.solve()
                timer.stop()


            if MPI.rank(mpi_comm_world()) == 0:
                print0("Time: {}".format(t))

            # Rollback
            u_assigner.assign(u0, U.sub(0))
            p_assigner.assign(p0, U.sub(1))

            yield t, u0, p0
