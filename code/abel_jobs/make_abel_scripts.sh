declare -a NOISES=(0 180 255 360)

declare -a FILES=(assimilate_aneurysm_obs_average_H1H1 assimilate_aneurysm_obs_average_l2l2 assimilate_aneurysm_obs_instant_H1H1 assimilate_aneurysm_obs_instant_l2l2)
DIR=generated
mkdir -p $DIR
for FILE in "${FILES[@]}"
do
    for NOISE in "${NOISES[@]}"
    do
        NEWFILE=$DIR/${FILE}_${NOISE}.job
        cp ${FILE}.job ${NEWFILE}
        sed -i "s/NOISEVALUE/${NOISE}/g" $NEWFILE
        echo "Created ${NEWFILE}"
    done
done
