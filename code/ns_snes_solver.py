"""Navier-Stokes solver using PETSc SNES. The linear solver uses a
preconditioning strategy for the Stokes equation.

"""
import sys
import petsc4py
from fenics import *
from dolfin_adjoint import *

from petsc4py import PETSc

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"


class GeneralProblem(NonlinearProblem):
  def __init__(self, F, z, bcs, J=None):
    NonlinearProblem.__init__(self)
    self.fform = F
    self.z = z
    self.bcs = bcs
    if J is None:
      J = derivative(F, z)
    self.jacobian = J

  def F(self, b, x):
    assemble(self.fform, tensor=b)
    [bc.apply(b, x) for bc in self.bcs]

  def J(self, A, x):
    assemble(self.jacobian, tensor=A)
    [bc.apply(A) for bc in self.bcs]


class NSSolver(object):

    def __init__(self, dim, facet_ids, wall_ids, controlled_facet_ids):
        self.dim = dim
        self.facet_ids = facet_ids
        self.controlled_facet_ids = controlled_facet_ids
        self.wall_ids = wall_ids


    def run(self, mesh, t_range, state0, g, nu=3.0,
                  nitsche_method = "symmetric",
                  nitsche_gamma = 10.,
                  convection = False,
                  nonlinear_method = "newton",
                  theta=0.5,
                  annotate=False):

        dt = t_range[1] - t_range[0]
        nu = Constant(nu)
        V = VectorFunctionSpace(mesh, "CG", 2)  # Velocity
        Q = FunctionSpace(mesh, "CG", 1)        # Pressure
        W = MixedFunctionSpace([V, Q])
        control_space = g.itervalues().next().function_space()

        v, q = TestFunctions(W)
        U = Function(W, name="Solution")
        u0 = Function(V, name="Velocity")
        u0.interpolate(state0[0], annotate=False)
        p0 = Function(Q, name="Pressure")
        p0.interpolate(state0[1], annotate=False)
        n = FacetNormal(mesh)

        if nonlinear_method == "picard":
            raise RuntimeError("Snes uses Newton's method.")
        else:
            u, p = split(U)
            beta = U

        # Define measures over mesh and facet_ids
        ds = Measure('ds', domain=mesh, subdomain_data=self.facet_ids)
        dx = Measure('dx', domain=mesh)

        # Create boundary conditions
        zero = (0,)*self.dim

        bcu_strong = [DirichletBC(W.sub(0), zero, self.facet_ids, id_)
                      for id_ in self.wall_ids]
        bcp_strong = [] # DirichletBC(W.sub(1), 0, facet_ids, inflow_id)

        # Weak pressure boundary condition
        bcp = []

        # Boundary measure where only weak bcs are defined
        ds_ = ds(self.controlled_facet_ids)

        # Define variational form
        u_theta = theta*u + (1-theta)*u0
        g_theta = Function(control_space, name="g_theta")

        # Standard Stokes terms
        F = (
            1.0/Constant(dt)*inner(u-u0,v)*dx()
             + nu*inner(grad(u_theta), grad(v))*dx()
             - nu*inner(grad(u_theta)*n, v)*ds_
             - inner(p, div(v))*dx()
             + inner(p*n, v)*ds_
             - inner(q, div(u))*dx()
             #+ inner(grad(q), u)*dx()
             #- q*inner(u, n)*ds_
            )
        # Add convection for Navier Stokes
        if convection and nonlinear_method == "picard":
            F += theta*inner(grad(u)*beta, v)*dx() + (1-theta)*inner(grad(u0)*u0, v)*dx()
        if convection and nonlinear_method == "newton":
            F += inner(grad(u_theta)*u_theta, v)*dx()

        # Pressure boundary conditions
        F += sum(dot(function*n, v)*ds(region) for (function, region) in bcp)

        # Weakly enforced boundary terms
        if nitsche_method == "symmetric":
            F += (
                - theta*nu*inner(grad(v)*n, u)*ds_
                + theta*nu*inner(grad(v)*n, g_theta)*ds_
                + inner(q*n, u)*ds_
                - inner(q*n, g_theta)*ds_
                )
        else:
            F += (
                - theta*nu*inner(grad(v)*n, u)*ds_
                + theta*nu*inner(grad(v)*n, g_theta)*ds_
                #+ inner(q*n, u)*ds_
                #- inner(q*n, g_theta)*ds_
                # Appears to stable without the last two terms here
                )

        # Add the Nitsche stabilization terms
        if nitsche_gamma > 0:
            h = CellSize(mesh)
            F +=(+ theta*nitsche_gamma*nu/h*inner(u,v)*ds_
                 - theta*nitsche_gamma*nu/h*inner(g_theta,v)*ds_)

        u_assigner = FunctionAssigner(V, W.sub(0))
        p_assigner = FunctionAssigner(Q, W.sub(1))

        # Extract lhs and rhs
        if nonlinear_method == "picard":
            a = lhs(F)
            L = rhs(F)

            b = assemble(L)
            A = assemble(a)

            # Fixed point tolerance
            FP_TOL = 1e-8

        # Initial condition
        yield t_range[0], u0, p0

        for t in t_range[1:]:
            # Update boundary condition
            g_theta.assign(g[t], annotate=annotate)
            
            # define the nonlinear problem for PETSc
            problem = GeneralProblem(F, U, bcs=bcu_strong + bcp_strong, J=derivative(F, U))
            
            # Set up the solver and fieldsplit
            solver = PETScSNESSolver()
            solver.parameters["report"] = False            
            snes = solver.snes()


            snes.setOptionsPrefix('sw_')
            solver.init(problem, U.vector())
            u_dofs = W.sub(0).dofmap().dofs()
            p_dofs = W.sub(1).dofmap().dofs()

            is0 = PETSc.IS().createGeneral(u_dofs)
            is1 = PETSc.IS().createGeneral(p_dofs)
            snes.ksp.pc.setFieldSplitIS(('0', is0), ('1', is1))

            
            def monitor(snes, its, norm):
              print "Inside monitor. its == %s" % its
              
              trial = TrialFunction(W)
              test  = TestFunction(W)
              
              time_term = (1.0 / dt) * inner(trial, test) * dx# taking account of the pressure time term in the lower-right block.
              #effective_friction = friction/depth
              #effective_friction = 2 * friction/depth * inner(u, u)**0.5 # change this to account for how the friction changes with quadratic drag
              #mass_coeff = 1/(1/dt + effective_friction)
              #laplace_term = g * depth * mass_coeff * inner(grad(trial[1]), grad(test[1])) * dx # the mass term in the velocity solve induces a Laplace-like term in the Schur complement
              
              schur_prec = assemble(time_term)# + laplace_term)
              
              [bc.apply(schur_prec) for bc in bcu_strong+bcp_strong]
              Zmat  = as_backend_type(schur_prec).mat()
              Pis   = PETSc.IS()
              Pis.createGeneral(p_dofs)
              Pmat  = Zmat.getSubMatrix(Pis, Pis)
              
              snes.ksp.pc.setFieldSplitSchurPreType(PETSc.PC.SchurPreType.USER, Pmat)

            if args == argv:
              snes.setMonitor(monitor)

            snes.solve(None, as_backend_type(U.vector()).vec())
          
          
            if MPI.rank(mpi_comm_world()) == 0:
                print "Time: ", t

            # Rollback
            u_assigner.assign(u0, U.sub(0))
            p_assigner.assign(p0, U.sub(1))

            yield t, u0, p0
