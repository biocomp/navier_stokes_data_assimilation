from dolfin import *

def TH(mesh):
    V = VectorFunctionSpace(mesh, "CG", 2)
    Q = FunctionSpace(mesh, "CG", 1)
    return V * Q 
    
def MINI(mesh):
    q = mesh.topology().dim() + 1
    V = VectorFunctionSpace(mesh, "CG", 1) \
      + VectorFunctionSpace(mesh, "Bubble", q)
    Q = FunctionSpace(mesh, "CG", 1)
    return V * Q
 
def P1P1(mesh):
    V = VectorFunctionSpace(mesh, "CG", 1)
    Q = FunctionSpace(mesh, "CG", 1)
    return V * Q 

def CR(mesh):
    V = VectorFunctionSpace(mesh, "CR", 1)
    Q = FunctionSpace(mesh, "DG", 0)
    return V * Q
    

def dim_TH(mesh):
    d = mesh.topology().dim()
    return (d + 1) * mesh.num_vertices() + d * mesh.num_edges()

def dim_MINI(mesh):
    d = mesh.topology().dim()
    return (d + 1) * mesh.num_vertices() + d * mesh.num_cells()

def dim_P1P1 (mesh):
    d = mesh.topology().dim()
    return (d + 1) * mesh.num_vertices()

def dim_CR(mesh): 
    d = mesh.topology().dim()
    return d * mesh.num_facets() + mesh.num_cells()

def compute_sizes(mesh):
    return [dim(mesh) for dim in [dim_TH, dim_MINI, dim_P1P1, dim_CR]]

def test(mesh = None):
    if mesh == None:
        Nx, Ny, Nz = 11, 12, 13
        mesh =  UnitCubeMesh(Nx, Ny, Nz)
    
    mesh.init()
    
    assert   TH(mesh).dim() == dim_TH(mesh)
    assert MINI(mesh).dim() == dim_MINI(mesh)
    assert P1P1(mesh).dim() == dim_P1P1(mesh)
    assert   CR(mesh).dim() == dim_CR(mesh)
    
    return True

def print_sizes(mesh):
    mesh.init(1)
    mesh.init(2)
    
    sizes  = compute_sizes(mesh)
    ratios = [float(size)/ sizes[0] for size in sizes]
    outstr = "\nComputed sizes for given mesh:\n"\
           + "------------------------------\n"\
           + "element    | absolute|relative\n"\
           + "------------------------------\n"\
           + "Taylor-Hood| {:8d}|{:8.2f}\n".format(sizes[0], ratios[0])\
           + "MINI       | {:8d}|{:8.2f}\n".format(sizes[1], ratios[1])\
           + "P1-P1      | {:8d}|{:8.2f}\n".format(sizes[2], ratios[2])\
           + "CR         | {:8d}|{:8.2f}\n".format(sizes[3], ratios[3])

    print outstr

    
if __name__ == "__main__":
    import sys
    fname = sys.argv[1]
    ext = fname.split(".")[-1]

    if ext == "xml":
        mesh = Mesh(fname)

    elif ext == "hdf5":
        print sys.argv[2]
        mesh = Mesh()
        f = HDF5File(mpi_comm_world(), fname, "r")
        f.read(mesh, sys.argv[2], False)
        f.close()
    
    else:
        print "\nUnhandled file extension: " + ext
        sys.exit()

    print_sizes(mesh)
        


