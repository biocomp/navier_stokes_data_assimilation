import sys
from collections import OrderedDict
#import pyipopt
from dolfin import *
from dolfin_adjoint import *
from problem import *
#import moola
from ns_solver import NSSolver
from utils import FileReader, FileWriter, ResultsWriter, print0
from linear_function import LinearFunction

parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(PROGRESS)

###################### Configure problem ############################
problem = create_problem_from_commandline()
print0(problem)

###################### Setup controls ############################
# Define the control function (inflow boundary condition)
# We control the boundary condition at every time level
print0("Set up control functions")
g = OrderedDict()
control_space = VectorFunctionSpace(problem.mesh, 'CG', 1)
t_old = problem.t_range[0]
for t in problem.t_range[1:]:
    t_theta = problem.theta*t + (1-problem.theta)*t_old
    if problem.dim == 3:
        g[t] = problem.u_bc.bc(t_theta)
    else:
        problem.u_bc.t = t_theta
        g[t] = interpolate(problem.u_bc, control_space, name="Control_{}".format(t))

    t_old = t

###################### Run forward model ############################
# Run the model to annotate dolfin-adjoint's tape, generate the synthetic
# observations, and build the form for the objective functional
print0("Run forward model")
ns_solver = NSSolver(problem.mesh, problem.dim, problem.facet_ids, problem.wall_ids, problem.controlled_facet_ids)
solver_params = {"nonlinear_method": "newton", "convection": True,
        "annotate": True, "nu": problem.nu,
        "nitsche_gamma": problem.nitsche_gamma, "theta": problem.theta,
        "velocity_space": problem.velocity_space}

# An integrator to calculate the temporal averages of the velocity
integrator = LinearFunction(problem.t_range, [None]*len(problem.t_range))

if problem.mode == "generate":
    results_writer = ResultsWriter(problem.t_range, problem.output_dir, obs_filename="obs")

u00 = Function(problem.u0)
for t, u, p in ns_solver.run(problem.t_range, problem.u0, g, **solver_params):

    # Project the velocity state into the observation space
    if args.mode == "generate":
        u_t = interpolate(u, problem.observation_space, name="u_projected_{}".format(t))
    else:
        u_t = project(u, u.function_space(), name="u_projected_{}".format(t))
    
    integrator.update(t, u_t)

    if problem.mode == "generate":
        results_writer.write_state(t, u, p, g)

# Print timings
set_log_level(INFO)
list_timings()
set_log_level(ERROR)

if problem.mode == "generate":
    results_writer.write_observations(integrator, problem.obs_t_range, problem.obs_averaged)
    sys.exit(0)

###################### Construct objective functional #########################

# Read observations
obs = OrderedDict()
fobs = FileReader("{}".format(problem.observation_file))
fobs_out = FileWriter("{}/obs".format(problem.output_dir))

if not problem.dog_obs:
    name = "obs_noisy"

    for i in range(len(problem.obs_t_range)-1):
        t0 = problem.obs_t_range[i]
        t1 = problem.obs_t_range[i+1]
        print0("Read observation for time range %f - %f (index %i)." % (
            t0, t1, i))
        obs[t0] = Function(problem.observation_space, annotate=False, name="obs_noisy{}".format(t0))
      
		fobs.read(obs[t0], "obs_noisy", t0)
        fobs_out.write(Function(obs[t0], name="obs_noisy"), "obs_noisy", t0)

else:
    V = VectorFunctionSpace(problem.mesh, "CG", 1)

    fobs = FileReader("{}/u150-ref{}/u150-ref{}".format(problem.observation_dir,
        problem.mesh_level, problem.mesh_level))
    fobs_out = FileWriter("{}/obs".format(problem.output_dir))

    for i, t in enumerate(problem.obs_t_range[:-1]):
        print0("Read observation for time range %f - %f (index %i)." % (t,
                problem.obs_t_range[i+1], i))
        obs[t] = Function(V, annotate=False, name="obs_{}".format(t))
        fobs.read(obs[t], "u150-ref{}{}".format(problem.mesh_level, i))
        fobs_out.write(Function(obs[t], name="obs"), "obs", t)


# Construct the objective functional
dx = Measure("dx", domain=problem.mesh)
j = Constant(0)*dx

for i in range(len(problem.obs_t_range)-1):
    a, b = problem.obs_t_range[i], problem.obs_t_range[i+1]
    if problem.obs_averaged:
        # Compute time average
        model_obs = integrator.integrate(a, b)/(b-a)
    else:
        # Evaluate pointwise
        model_obs = integrator(a)

    # restrict integral measure to observed subdomain
    _dx = Measure("dx", domain = problem.mesh, subdomain_data = problem.cell_ids)
    
    # Add the difference to the objective functional
    j += 0.5*inner(model_obs - obs[a], model_obs - obs[a])*_dx(problem.obs_domain_ids)

# H1 regularisation in time
time_regularisation = sum([(g1-g0)**2 for g1, g0 in zip(g.values()[:-1], g.values()[1:])])
ds_ = ns_solver.ds(ns_solver.controlled_facet_ids)
dt = problem.t_range[1] - problem.t_range[0]
j += Constant(0.5 * problem.time_alpha/dt) * time_regularisation*ds_

# Enforce periodicity via quadratic penalty term
periodic_regularisation = (g.values()[0]-u)**2*dx
j += Constant(0.5 * problem.periodicity_alpha) * periodic_regularisation

# Scale functional
scale = 1
j = Constant(scale)*j
J = Functional(j)


###################### Optimisation setup ############################
parameters["adjoint"]["stop_annotating"] = True

# Define the dolfin-adjoint controls
c = [Control(problem.u0)]  # initial condition
c += [Control(gg) for gg in g.itervalues()]  # bc conditions

# Compute and plot the gradient (optional)
#dJ = compute_gradient(J, c, project=False, forget=False)
#for i, t in enumerate(problem.t_range):
#   plot(dJ[i], title="dJ, t = %f" % t)
#    interactive()

# Set up the optimization
def jhat_eval_pre(*args):
    print0("Running forward model...")
def jhat_eval_post(j, *args):
    print0(" j = {:.2e}".format(j))
def jhat_deriv(*args):
    print0("Running adjoint model")
def replay_cb(var, value, m):
    if "Control" in var.var.name:
        maxval = MPI.max(mpi_comm_world(), max(abs((value.vector().array()))))
        print0(" max({}) = {}".format(var.var.name, maxval))

Jhat = ReducedFunctional(J, c, eval_cb_pre=jhat_eval_pre,
                               eval_cb_post=jhat_eval_post,
                               derivative_cb_pre=jhat_deriv,
                               replay_cb=replay_cb)

# Perform a sanity check. Test that the manually computed functional value
# is equal to the evaluation of the reduced functional for the same inputs
ctrl_vals = [u00] + g.values()
assert abs(Jhat(ctrl_vals) - assemble(j)) < 1e-5/scale

###################### Optimize ############################
opt = minimize(Jhat, options={"maxiter": problem.opt_maxiter})
#problem = MinimizationProblem(Jhat)
#parameters = {"acceptable_tol": 1.0e-200, "maximum_iterations": 15,
#              "print_level": 5}
#solver = IPOPTSolver(problem, parameters=parameters)
#g_opt = solver.solve()

# Split optimal solution in its components
u0_opt = opt[0]
g_opt = OrderedDict(zip(problem.t_range[1:], opt[1:]))

# Write optimised results
solver_params["annotate"] = False
results_writer = ResultsWriter(problem.t_range, problem.output_dir)

for t, u, p in ns_solver.run(problem.t_range, u0_opt, g_opt, **solver_params):
    # Project the velocity state into the observation space
    u_t = project(u, problem.observation_space, name="u_optimal_{}".format(t))
    integrator.update(t, u_t)
    results_writer.write_state(t, u, p, g_opt)

results_writer.write_observations(integrator, problem.obs_t_range, problem.obs_averaged)
print0("Periodicity regularisation {}".format(assemble(problem.periodicity_alpha * (g_opt.values()[0]-u)**2*dx)))
print0("H1-time regularisation {}".format(assemble(problem.time_alpha*sum([inner(g1-g0, g1-g0)*dx for g1, g0 in zip(g_opt.values()[:-1], g_opt.values()[1:])]))))
