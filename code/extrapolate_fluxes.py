import argparse
import matplotlib.pyplot as plt
from collections import OrderedDict
from numpy import arange, linspace
from dolfin import *
from utils import FileReader, print0
from scipy.interpolate import interp1d

# Read the command line arguments
example = "Example arguments: --observation_dir=assimilation_data/VENC150_wide --mesh_level 3"
parser = argparse.ArgumentParser(description=example)
parser.add_argument('--observation_dir', required=True,
        help='path with observations')
parser.add_argument('--mesh_level', type=int, help='mesh level', required=True)
args = parser.parse_args()

# Read the mesh
mesh = Mesh()
mesh_file = "assimilation_data/meshes_wide/model.hdf5"
f =  HDF5File(mpi_comm_world(), mesh_file, "r")
f.read(mesh, "Meshes/Mesh{}".format(args.mesh_level), False)

# Read facet ids
facet_mvc = MeshValueCollection("size_t", mesh, 2)
f.read(facet_mvc, "Meshes/Mesh{}/MeshValueCollections/facet_domains".format(args.mesh_level))
facet_ids = MeshFunction("size_t", mesh, facet_mvc)
del f

# Read observations
fobs = FileReader("{}/u150-ref{}/u150-ref{}".format(args.observation_dir,
    args.mesh_level, args.mesh_level))

V = VectorFunctionSpace(mesh, "CG", 1)
obs = OrderedDict()
obs_dt = 0.0370
T = 17 * obs_dt + DOLFIN_EPS
obs_t_range = arange(0, T, obs_dt)
for i, t in enumerate(obs_t_range[:-1]):
    print0("Read observation for time range %f - %f (index %i)." % (t,
            obs_t_range[i+1], i))
    obs[t] = Function(V, annotate=False, name="obs_{}".format(t))
    fobs.read(obs[t], "u150-ref{}{}".format(args.mesh_level, i))


# Compute fluxes through boundaries
inflow_id = 3
outflow_left_id = 2
outflow_right_id = 1

dx = Measure('dx', domain=mesh)
ds = Measure('ds', domain=mesh, subdomain_data=facet_ids)

flux_fun = {}
n = FacetNormal(mesh)
for i in range(1,4):
    fluxes = [assemble(-inner(o, n)*ds(i)) for o in obs.values()]
    flux_fun[i] = interp1d(obs_t_range[:-1], fluxes) #, kind='cubic')

# Plot fluxes
tplot = linspace(obs_t_range[0], obs_t_range[-2], 100)
plt.plot(tplot, flux_fun[3](tplot),'-', label="in")
plt.plot(tplot, -flux_fun[1](tplot),'-', label="out right")
plt.plot(tplot, -flux_fun[2](tplot),'-', label="out left")
plt.plot(tplot, -flux_fun[1](tplot)-flux_fun[2](tplot),'--', label="out left + right")
plt.legend()

# Fill between linex
plt.fill_between(tplot, flux_fun[3](tplot), -flux_fun[1](tplot)-flux_fun[2](tplot), color='grey', alpha='0.2')

plt.xlabel('time (s)')
plt.ylabel(r'flux (mm$^3$/s)')
plt.yticks(arange(0, 8000, 2000))
plt.xticks(arange(0, 0.7, 0.2))
plt.xlim(0, 0.6)

plt.savefig("fluxes.pdf")
#plt.show()

print "Outflow right flux"
print "{",
for t in obs_t_range[:-1]:
    print "{}: {},".format(t, flux_fun[1](t)),
print "}"
print "Outflow left flux"
print "{",
for t in obs_t_range[:-1]:
    print "{}: {},".format(t, flux_fun[2](t)),
print "}"
print "Inflow flux"
print "{",
for t in obs_t_range[:-1]:
    print "{}: {},".format(t, flux_fun[3](t)),
print "}"
