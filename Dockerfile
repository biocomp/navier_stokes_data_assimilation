FROM quay.io/fenicsproject/stable:2016.1.0
MAINTAINER Simon Funke <simon@simula.no>

USER root
RUN git clone https://bitbucket.org/dolfin-adjoint/libadjoint.git && cd libadjoint && git checkout libadjoint-2016.2.0 && mkdir build && cd build && cmake .. && make install
RUN pip2 install git+https://github.com/funsim/moola.git@master 
RUN pip2 install git+https://bitbucket.org/dolfin-adjoint/dolfin-adjoint.git@dolfin-adjoint-2016.2.0 

USER root
