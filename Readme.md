Install
-------

See INSTALL file

Introduction
------------

This repository contains the source code and input files to reproduce the
results of the paper

S. W. Funke, M. Nordaas, {\O}. Evju, M. S. Aln{\ae}s, K.-A. Mardal,
Variational data assimilation for transient blood flow simulations

Software dependencies
---------------------
* FEniCS 2016.1  (www.fenicsproject.org)
* dolfin-adjoint 2016.1  (www.dolfin-adjoint.org)
* Moola (https://github.com/funsim/moola)

The easiest installation is by using Docker, see the INSTALL file

How to reproduce results
------------------------

* Section 3.4 Verification

    ```bash
    $ cd code
    ```

    Generate the observations
    ```bash
    $ ./scripts/generate_aneurysm_obs_instant.sh
    ```
    The observations will be stored in `code/results_aneurysm/instant/obs`.

    Run the taylor test with
    ```bash
    $ NOISE=0 ./scripts/taylor_test_instant.sh
    ```



* Section 4.1 2D Aneurysm

    ```bash
    $ cd code
    ```

    Generate the observations
    ```bash
    $ ./scripts/generate_aneurysm_obs_instant.sh
    ```

    The observations will be stored in `code/results_aneurysm/instant/obs`.

    To run the data assimilation, one executes
    ```bash
    $ NOISE=0 ./scripts/assimilate_aneurysm_obs_instant_H1H1.sh
    ```

    The NOISE value can be one of 0, 180, 255 and 360.

	The above scripts applies the instantaneous observation operator. For the
    averaged observation operators, one uses the scripts
    `generate_aneurysm_obs_averaged.sh` and
    `assimilate_aneurysm_obs_averaged_H1H1.sh`.

* Section 4.1.1 Sensitivity of reconstruction with respect to parameter changes

    The results of this section are produced by varying the above settings in
    the `Aneurysm2D` class in `problem.py`.
